--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categories (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    parent_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    "order" integer NOT NULL
);


ALTER TABLE public.categories OWNER TO postgres;

--
-- Name: categories_en; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categories_en (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.categories_en OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_id_seq OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- Name: categories_ru; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categories_ru (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.categories_ru OWNER TO postgres;

--
-- Name: country; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE country (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    code character varying(255) NOT NULL
);


ALTER TABLE public.country OWNER TO postgres;

--
-- Name: country_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_id_seq OWNER TO postgres;

--
-- Name: country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE country_id_seq OWNED BY country.id;


--
-- Name: history; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE history (
    id integer NOT NULL,
    user_id integer NOT NULL,
    order_id integer NOT NULL,
    state character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.history OWNER TO postgres;

--
-- Name: history_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.history_id_seq OWNER TO postgres;

--
-- Name: history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE history_id_seq OWNED BY history.id;


--
-- Name: invests; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE invests (
    id integer NOT NULL,
    user_id integer NOT NULL,
    term integer NOT NULL,
    percents integer NOT NULL,
    amount double precision NOT NULL,
    state character varying(255) DEFAULT 'waiting'::character varying NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    start timestamp(0) without time zone,
    package_id integer NOT NULL,
    term_id integer NOT NULL
);


ALTER TABLE public.invests OWNER TO postgres;

--
-- Name: invests_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE invests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.invests_id_seq OWNER TO postgres;

--
-- Name: invests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE invests_id_seq OWNED BY invests.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE migrations_id_seq OWNED BY migrations.id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE orders (
    id integer NOT NULL,
    product_id integer NOT NULL,
    amount double precision NOT NULL,
    state character varying(255) NOT NULL,
    comment text,
    user_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.orders OWNER TO postgres;

--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orders_id_seq OWNER TO postgres;

--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE orders_id_seq OWNED BY orders.id;


--
-- Name: package; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE package (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    amount_min integer NOT NULL,
    amount_max integer NOT NULL,
    description text NOT NULL,
    sort_order integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    monthly boolean DEFAULT false NOT NULL
);


ALTER TABLE public.package OWNER TO postgres;

--
-- Name: package_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE package_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.package_id_seq OWNER TO postgres;

--
-- Name: package_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE package_id_seq OWNED BY package.id;


--
-- Name: package_term; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE package_term (
    id integer NOT NULL,
    package_id integer NOT NULL,
    term integer NOT NULL,
    percents integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.package_term OWNER TO postgres;

--
-- Name: package_term_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE package_term_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.package_term_id_seq OWNER TO postgres;

--
-- Name: package_term_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE package_term_id_seq OWNED BY package_term.id;


--
-- Name: pages; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pages (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    alias character varying(255) NOT NULL,
    body text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.pages OWNER TO postgres;

--
-- Name: pages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pages_id_seq OWNER TO postgres;

--
-- Name: pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pages_id_seq OWNED BY pages.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- Name: payouts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE payouts (
    id integer NOT NULL,
    user_id integer NOT NULL,
    done boolean DEFAULT false NOT NULL,
    amount double precision NOT NULL,
    requisites character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    comment text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    package_id integer NOT NULL,
    term_id integer NOT NULL
);


ALTER TABLE public.payouts OWNER TO postgres;

--
-- Name: payouts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE payouts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payouts_id_seq OWNER TO postgres;

--
-- Name: payouts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE payouts_id_seq OWNED BY payouts.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE products (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text NOT NULL,
    price double precision NOT NULL,
    sort_order integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    type character varying(255) DEFAULT 'miner'::character varying NOT NULL,
    category_id integer
);


ALTER TABLE public.products OWNER TO postgres;

--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO postgres;

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: role_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE role_user (
    role_id integer NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.role_user OWNER TO postgres;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    surname character varying(255) NOT NULL,
    middle_name character varying(255),
    country_id integer NOT NULL,
    phone character varying(255) NOT NULL,
    viber character varying(255),
    skype character varying(255),
    referal_id integer DEFAULT 1 NOT NULL,
    blocked boolean DEFAULT false NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY country ALTER COLUMN id SET DEFAULT nextval('country_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY history ALTER COLUMN id SET DEFAULT nextval('history_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invests ALTER COLUMN id SET DEFAULT nextval('invests_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations ALTER COLUMN id SET DEFAULT nextval('migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orders ALTER COLUMN id SET DEFAULT nextval('orders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY package ALTER COLUMN id SET DEFAULT nextval('package_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY package_term ALTER COLUMN id SET DEFAULT nextval('package_term_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pages ALTER COLUMN id SET DEFAULT nextval('pages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY payouts ALTER COLUMN id SET DEFAULT nextval('payouts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY categories (id, name, parent_id, created_at, updated_at, "order") FROM stdin;
0	root	\N	2017-12-07 17:50:59	2017-12-07 17:50:59	-1
2	Изобретения Ноу-Хау	0	2017-12-28 19:49:56	2017-12-28 19:49:56	2000
3	Товары для Здоровья	0	2017-12-28 23:56:39	2017-12-28 23:56:39	3000
4	Товары для Красоты	0	2017-12-28 23:57:25	2017-12-28 23:57:25	4000
1	Поисковое Оборудование	0	2017-12-28 19:47:57	2017-12-29 00:09:19	700
5	Товары для Трейдеров и Инвесторов	0	2017-12-29 00:11:05	2017-12-29 00:11:05	500
\.


--
-- Data for Name: categories_en; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY categories_en (id, name) FROM stdin;
1	Георадарное Оборудование
\.


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categories_id_seq', 5, true);


--
-- Data for Name: categories_ru; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY categories_ru (id, name) FROM stdin;
\.


--
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY country (id, name, created_at, updated_at, code) FROM stdin;
1	United States	\N	\N	US
2	Canada	\N	\N	CA
3	Afghanistan	\N	\N	AF
4	Albania	\N	\N	AL
5	Algeria	\N	\N	DZ
6	American Samoa	\N	\N	AS
7	Andorra	\N	\N	AD
8	Angola	\N	\N	AO
9	Anguilla	\N	\N	AI
10	Antarctica	\N	\N	AQ
11	Antigua and/or Barbuda	\N	\N	AG
12	Argentina	\N	\N	AR
13	Armenia	\N	\N	AM
14	Aruba	\N	\N	AW
15	Australia	\N	\N	AU
16	Austria	\N	\N	AT
17	Azerbaijan	\N	\N	AZ
18	Bahamas	\N	\N	BS
19	Bahrain	\N	\N	BH
20	Bangladesh	\N	\N	BD
21	Barbados	\N	\N	BB
22	Belarus	\N	\N	BY
23	Belgium	\N	\N	BE
24	Belize	\N	\N	BZ
25	Benin	\N	\N	BJ
26	Bermuda	\N	\N	BM
27	Bhutan	\N	\N	BT
28	Bolivia	\N	\N	BO
29	Bosnia and Herzegovina	\N	\N	BA
30	Botswana	\N	\N	BW
31	Bouvet Island	\N	\N	BV
32	Brazil	\N	\N	BR
33	British lndian Ocean Territory	\N	\N	IO
34	Brunei Darussalam	\N	\N	BN
35	Bulgaria	\N	\N	BG
36	Burkina Faso	\N	\N	BF
37	Burundi	\N	\N	BI
38	Cambodia	\N	\N	KH
39	Cameroon	\N	\N	CM
40	Cape Verde	\N	\N	CV
41	Cayman Islands	\N	\N	KY
42	Central African Republic	\N	\N	CF
43	Chad	\N	\N	TD
44	Chile	\N	\N	CL
45	China	\N	\N	CN
46	Christmas Island	\N	\N	CX
47	Cocos (Keeling) Islands	\N	\N	CC
48	Colombia	\N	\N	CO
49	Comoros	\N	\N	KM
50	Congo	\N	\N	CG
51	Cook Islands	\N	\N	CK
52	Costa Rica	\N	\N	CR
53	Croatia (Hrvatska)	\N	\N	HR
54	Cuba	\N	\N	CU
55	Cyprus	\N	\N	CY
56	Czech Republic	\N	\N	CZ
57	Democratic Republic of Congo	\N	\N	CD
58	Denmark	\N	\N	DK
59	Djibouti	\N	\N	DJ
60	Dominica	\N	\N	DM
61	Dominican Republic	\N	\N	DO
62	East Timor	\N	\N	TP
63	Ecudaor	\N	\N	EC
64	Egypt	\N	\N	EG
65	El Salvador	\N	\N	SV
66	Equatorial Guinea	\N	\N	GQ
67	Eritrea	\N	\N	ER
68	Estonia	\N	\N	EE
69	Ethiopia	\N	\N	ET
70	Falkland Islands (Malvinas)	\N	\N	FK
71	Faroe Islands	\N	\N	FO
72	Fiji	\N	\N	FJ
73	Finland	\N	\N	FI
74	France	\N	\N	FR
75	France, Metropolitan	\N	\N	FX
76	French Guiana	\N	\N	GF
77	French Polynesia	\N	\N	PF
78	French Southern Territories	\N	\N	TF
79	Gabon	\N	\N	GA
80	Gambia	\N	\N	GM
81	Georgia	\N	\N	GE
82	Germany	\N	\N	DE
83	Ghana	\N	\N	GH
84	Gibraltar	\N	\N	GI
85	Greece	\N	\N	GR
86	Greenland	\N	\N	GL
87	Grenada	\N	\N	GD
88	Guadeloupe	\N	\N	GP
89	Guam	\N	\N	GU
90	Guatemala	\N	\N	GT
91	Guinea	\N	\N	GN
92	Guinea-Bissau	\N	\N	GW
93	Guyana	\N	\N	GY
94	Haiti	\N	\N	HT
95	Heard and Mc Donald Islands	\N	\N	HM
96	Honduras	\N	\N	HN
97	Hong Kong	\N	\N	HK
98	Hungary	\N	\N	HU
99	Iceland	\N	\N	IS
100	India	\N	\N	IN
101	Indonesia	\N	\N	ID
102	Iran (Islamic Republic of)	\N	\N	IR
103	Iraq	\N	\N	IQ
104	Ireland	\N	\N	IE
105	Israel	\N	\N	IL
106	Italy	\N	\N	IT
107	Ivory Coast	\N	\N	CI
108	Jamaica	\N	\N	JM
109	Japan	\N	\N	JP
110	Jordan	\N	\N	JO
111	Kazakhstan	\N	\N	KZ
112	Kenya	\N	\N	KE
113	Kiribati	\N	\N	KI
114	Korea, Democratic People's Republic of	\N	\N	KP
115	Korea, Republic of	\N	\N	KR
116	Kuwait	\N	\N	KW
117	Kyrgyzstan	\N	\N	KG
118	Lao People's Democratic Republic	\N	\N	LA
119	Latvia	\N	\N	LV
120	Lebanon	\N	\N	LB
121	Lesotho	\N	\N	LS
122	Liberia	\N	\N	LR
123	Libyan Arab Jamahiriya	\N	\N	LY
124	Liechtenstein	\N	\N	LI
125	Lithuania	\N	\N	LT
126	Luxembourg	\N	\N	LU
127	Macau	\N	\N	MO
128	Macedonia	\N	\N	MK
129	Madagascar	\N	\N	MG
130	Malawi	\N	\N	MW
131	Malaysia	\N	\N	MY
132	Maldives	\N	\N	MV
133	Mali	\N	\N	ML
134	Malta	\N	\N	MT
135	Marshall Islands	\N	\N	MH
136	Martinique	\N	\N	MQ
137	Mauritania	\N	\N	MR
138	Mauritius	\N	\N	MU
139	Mayotte	\N	\N	TY
140	Mexico	\N	\N	MX
141	Micronesia, Federated States of	\N	\N	FM
142	Moldova, Republic of	\N	\N	MD
143	Monaco	\N	\N	MC
144	Mongolia	\N	\N	MN
145	Montserrat	\N	\N	MS
146	Morocco	\N	\N	MA
147	Mozambique	\N	\N	MZ
148	Myanmar	\N	\N	MM
149	Namibia	\N	\N	NA
150	Nauru	\N	\N	NR
151	Nepal	\N	\N	NP
152	Netherlands	\N	\N	NL
153	Netherlands Antilles	\N	\N	AN
154	New Caledonia	\N	\N	NC
155	New Zealand	\N	\N	NZ
156	Nicaragua	\N	\N	NI
157	Niger	\N	\N	NE
158	Nigeria	\N	\N	NG
159	Niue	\N	\N	NU
160	Norfork Island	\N	\N	NF
161	Northern Mariana Islands	\N	\N	MP
162	Norway	\N	\N	NO
163	Oman	\N	\N	OM
164	Pakistan	\N	\N	PK
165	Palau	\N	\N	PW
166	Panama	\N	\N	PA
167	Papua New Guinea	\N	\N	PG
168	Paraguay	\N	\N	PY
169	Peru	\N	\N	PE
170	Philippines	\N	\N	PH
171	Pitcairn	\N	\N	PN
172	Poland	\N	\N	PL
173	Portugal	\N	\N	PT
174	Puerto Rico	\N	\N	PR
175	Qatar	\N	\N	QA
176	Republic of South Sudan	\N	\N	SS
177	Reunion	\N	\N	RE
178	Romania	\N	\N	RO
179	Russian Federation	\N	\N	RU
180	Rwanda	\N	\N	RW
181	Saint Kitts and Nevis	\N	\N	KN
182	Saint Lucia	\N	\N	LC
183	Saint Vincent and the Grenadines	\N	\N	VC
184	Samoa	\N	\N	WS
185	San Marino	\N	\N	SM
186	Sao Tome and Principe	\N	\N	ST
187	Saudi Arabia	\N	\N	SA
188	Senegal	\N	\N	SN
189	Serbia	\N	\N	RS
190	Seychelles	\N	\N	SC
191	Sierra Leone	\N	\N	SL
192	Singapore	\N	\N	SG
193	Slovakia	\N	\N	SK
194	Slovenia	\N	\N	SI
195	Solomon Islands	\N	\N	SB
196	Somalia	\N	\N	SO
197	South Africa	\N	\N	ZA
198	South Georgia South Sandwich Islands	\N	\N	GS
199	Spain	\N	\N	ES
200	Sri Lanka	\N	\N	LK
201	St. Helena	\N	\N	SH
202	St. Pierre and Miquelon	\N	\N	PM
203	Sudan	\N	\N	SD
204	Suriname	\N	\N	SR
205	Svalbarn and Jan Mayen Islands	\N	\N	SJ
206	Swaziland	\N	\N	SZ
207	Sweden	\N	\N	SE
208	Switzerland	\N	\N	CH
209	Syrian Arab Republic	\N	\N	SY
210	Taiwan	\N	\N	TW
211	Tajikistan	\N	\N	TJ
212	Tanzania, United Republic of	\N	\N	TZ
213	Thailand	\N	\N	TH
214	Togo	\N	\N	TG
215	Tokelau	\N	\N	TK
216	Tonga	\N	\N	TO
217	Trinidad and Tobago	\N	\N	TT
218	Tunisia	\N	\N	TN
219	Turkey	\N	\N	TR
220	Turkmenistan	\N	\N	TM
221	Turks and Caicos Islands	\N	\N	TC
222	Tuvalu	\N	\N	TV
223	Uganda	\N	\N	UG
224	Ukraine	\N	\N	UA
225	United Arab Emirates	\N	\N	AE
226	United Kingdom	\N	\N	GB
227	United States minor outlying islands	\N	\N	UM
228	Uruguay	\N	\N	UY
229	Uzbekistan	\N	\N	UZ
230	Vanuatu	\N	\N	VU
231	Vatican City State	\N	\N	VA
232	Venezuela	\N	\N	VE
233	Vietnam	\N	\N	VN
234	Virgin Islands (British)	\N	\N	VG
235	Virgin Islands (U.S.)	\N	\N	VI
236	Wallis and Futuna Islands	\N	\N	WF
237	Western Sahara	\N	\N	EH
238	Yemen	\N	\N	YE
239	Yugoslavia	\N	\N	YU
240	Zaire	\N	\N	ZR
241	Zambia	\N	\N	ZM
242	Zimbabwe	\N	\N	ZW
\.


--
-- Name: country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('country_id_seq', 242, true);


--
-- Data for Name: history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY history (id, user_id, order_id, state, created_at, updated_at) FROM stdin;
5	7	3	payed	2017-12-15 15:11:55	2017-12-15 15:11:55
6	4	4	payed	2017-12-28 23:50:25	2017-12-28 23:50:25
7	3	5	payed	2017-12-29 00:16:34	2017-12-29 00:16:34
\.


--
-- Name: history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('history_id_seq', 7, true);


--
-- Data for Name: invests; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY invests (id, user_id, term, percents, amount, state, created_at, updated_at, start, package_id, term_id) FROM stdin;
13	3	90	30	500	waiting	2017-12-07 18:15:58	2017-12-07 18:15:58	\N	4	7
14	3	90	30	500	waiting	2017-12-07 18:17:06	2017-12-07 18:17:06	\N	4	7
15	3	120	700	10000	waiting	2017-12-07 18:21:11	2017-12-07 18:21:11	\N	2	6
16	3	120	700	10000	waiting	2017-12-07 18:21:53	2017-12-07 18:21:53	\N	2	6
\.


--
-- Name: invests_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('invests_id_seq', 41, true);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2016_03_21_190857_Role	1
4	2016_03_25_080344_Pages	1
5	2017_10_23_145100_CREATE_PACKAGE	1
6	2017_10_23_145519_CREATE_PACKAGE_TERM	1
7	2017_10_23_150037_CREATE_COUNTRY	1
8	2017_10_23_150138_ADD_PROFILE	1
9	2017_10_26_091031_ChangeCountryTable	1
10	2017_11_06_064435_Invests	2
11	2017_11_06_075114_EditStartInvests	2
12	2017_11_06_075403_EditStartInvests2	2
13	2017_11_06_083219_Items	2
14	2017_11_06_083559_Products	2
15	2017_11_06_083639_removeItems	2
16	2017_11_07_234759_Referals	3
17	2017_11_08_141520_Orders	3
18	2017_11_08_141850_OrderHistory	3
19	2017_11_08_151337_EditProducts	3
20	2017_11_08_152653_Payout	3
21	2017_11_14_101434_EditPayout	4
22	2017_11_14_101756_EditPackage	4
23	2017_11_14_111211_EditInvest	4
24	2016_03_26_082731_Categories	5
25	2016_04_01_143424_add_fields_to_categories_table	5
26	2016_04_01_151911_add_category_to_products_table	5
27	2016_04_20_182314_Category_en_ru	5
28	2017_12_07_113855_UserBlock	5
\.


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('migrations_id_seq', 28, true);


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY orders (id, product_id, amount, state, comment, user_id, created_at, updated_at) FROM stdin;
3	1	3500	payed	\N	7	2017-12-15 15:11:55	2017-12-15 15:11:55
4	1	3500	payed	\N	4	2017-12-28 23:50:25	2017-12-28 23:50:25
5	2	4000	payed	\N	3	2017-12-29 00:16:34	2017-12-29 00:16:34
\.


--
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('orders_id_seq', 5, true);


--
-- Data for Name: package; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY package (id, name, amount_min, amount_max, description, sort_order, created_at, updated_at, monthly) FROM stdin;
2	ЗОЛОТО	5000	20000	<p>Это описание пакета и что вы получите в итоге</p>	2	2017-11-06 09:04:29	2017-12-28 19:32:26	f
4	СЕРЕБРО	100	5000	<p>Пакет Серебро</p>	3	2017-11-22 13:47:58	2017-12-28 19:52:59	f
1	ПЛАТИНА	20000	10000	<p>Если вы вложили $500, то через 90 дней у вас будет $650. Через 180<br />\r\nдней будет $800. Через 365 дней у вас будет $1100. Начисления процентов на Ваш счет<br />\r\nидет ежедневно. Вывод процентов и также тела вклада возможен по истечении периода<br />\r\nинвестиции.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>	1	2017-11-02 16:36:03	2017-12-28 19:55:00	f
\.


--
-- Name: package_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('package_id_seq', 6, true);


--
-- Data for Name: package_term; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY package_term (id, package_id, term, percents, created_at, updated_at) FROM stdin;
1	1	30	120	2017-11-06 09:02:05	2017-11-06 09:02:05
2	1	90	200	2017-11-06 09:02:12	2017-11-06 09:02:12
3	1	120	500	2017-11-06 09:02:18	2017-11-06 09:02:18
4	2	30	200	2017-11-06 09:04:39	2017-11-06 09:04:39
5	2	90	500	2017-11-06 09:04:43	2017-11-06 09:04:43
6	2	120	700	2017-11-06 09:04:48	2017-11-06 09:04:48
7	4	90	30	2017-11-22 13:49:17	2017-11-22 13:49:17
\.


--
-- Name: package_term_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('package_term_id_seq', 11, true);


--
-- Data for Name: pages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pages (id, title, alias, body, created_at, updated_at) FROM stdin;
\.


--
-- Name: pages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pages_id_seq', 1, false);


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: payouts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY payouts (id, user_id, done, amount, requisites, type, comment, created_at, updated_at, package_id, term_id) FROM stdin;
\.


--
-- Name: payouts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('payouts_id_seq', 1, true);


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products (id, name, description, price, sort_order, created_at, updated_at, type, category_id) FROM stdin;
1	Вычислитель на Видеокартах - Ферма	<p><strong>CIG</strong> <strong>GPU</strong> <strong>MINER</strong><strong> 6 х </strong><strong>RX</strong><strong>470 169-173</strong><strong>MH</strong><strong>/</strong><strong>s</strong></p>\r\n\r\n<p><strong>Спецификация:</strong></p>\r\n\r\n<p>1. &nbsp;6 х Видеокарт - Radeon Sapphire RX 470 (на лучших чипах Самсунга)</p>\r\n\r\n<p>2. &nbsp;Материнская плата - Asus Prime&nbsp;</p>\r\n\r\n<p>3. &nbsp;Оперативная память - ОЗУ DDR 8GB&nbsp;</p>\r\n\r\n<p>4.&nbsp; Процессор - Intel Celeron</p>\r\n\r\n<p>5.&nbsp; Жесткий диск - SSD 60-120 GB</p>\r\n\r\n<p>6.&nbsp; Блок Питания - Chieftec 1250 W 80 Gold&nbsp;</p>\r\n\r\n<p>7. &nbsp;6 х Райзеров - T-Riser PCI-E 1x to 16x 60cm v.06&nbsp;</p>\r\n\r\n<p>8.&nbsp; Эмулятор монитора - активный переходник HDMI-VGA 108</p>\r\n\r\n<p>9.&nbsp; USB watchdog</p>\r\n\r\n<p>10.&nbsp;Стойка - Алюминий&nbsp;</p>\r\n\r\n<p>11. Потребление: 900W&nbsp; (около 700-1000 грн. в мес.)</p>\r\n\r\n<p>12. Мощность добычи:&nbsp; ETH 169-176 MH/s / DCR 2600 MH/s / Zcash 1900-2010 MH/s&nbsp;</p>\r\n\r\n<p>ЕСТЬ&nbsp; В НАЛИЧИИ !!!</p>	3500	1	2017-11-06 09:02:41	2017-12-29 00:04:28	miner	\N
2	Вычислитель на Чипах - Айсик	<p><strong>CIG &nbsp;ANTMINER L3+ &nbsp;504 MH/s Scrypt ASIC</strong></p>\r\n\r\n<p>Спецификации:</p>\r\n\r\n<p>1. Хэшрейт (скорость работы):&nbsp; 504 Мh/с &plusmn;5%</p>\r\n\r\n<p>2. Потребляемая мощность: 800W +10% (при эффективности источника&nbsp; питания 93% и температуре в помещении 25 &deg; С)</p>\r\n\r\n<p>3. Энергоэффективность: 1.6 Дж/Мh+10%</p>\r\n\r\n<p>4. Напряжение питания: 12 V +5%</p>\r\n\r\n<p>5. Интерфейс электропитания: десять 6-пиновых разъема типа PCI-E</p>\r\n\r\n<p>6. Количество чипов на устройстве:&nbsp; 288 штук x BM1485</p>\r\n\r\n<p>7. Габариты: 352mm (Д) x 130mm (Ш) x 187.5mm (В)</p>\r\n\r\n<p>8. Вес:&nbsp; Нетто 6.5 кг</p>\r\n\r\n<p>9. Охлаждение: &nbsp; 2 вентилятора по 120мм на 38мм толщиной, 5500 rpm</p>\r\n\r\n<p>10. Рабочая температура: от 0 &deg; C до 40 &deg; C</p>\r\n\r\n<p>11. Сетевое подключение:&nbsp; Разъем Ethernet</p>\r\n\r\n<p>12. Шум:&nbsp; 75 дБ на расстоянии 1 м</p>\r\n\r\n<p>13. Гарантия &ndash; 180 дней</p>\r\n\r\n<p>11. Стабильный ежедневный доход в LTC</p>\r\n\r\n<p>ЕСТЬ В НАЛИЧИИ !!!</p>	4000	2	2017-11-06 09:02:59	2017-12-29 00:07:46	miner	\N
\.


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_id_seq', 3, true);


--
-- Data for Name: role_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY role_user (role_id, user_id, created_at, updated_at) FROM stdin;
1	3	\N	\N
1	8	\N	\N
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY roles (id, name, created_at, updated_at) FROM stdin;
2	Moderator	2017-11-02 15:08:33	2017-11-02 15:08:33
4	User	2017-11-02 15:08:33	2017-11-02 15:08:33
1	Administrator	2017-11-02 15:08:33	2017-12-29 00:12:27
\.


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('roles_id_seq', 1, false);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, name, email, password, remember_token, created_at, updated_at, surname, middle_name, country_id, phone, viber, skype, referal_id, blocked) FROM stdin;
2	User	user@mail.com	$2y$10$WExZPYSRcnbkBxkc4Tvn/uzllY24VxAmKSyvVqPAezqD6pCHoIEra	\N	2017-11-09 12:29:51	2017-11-09 12:29:51	UserName	\N	1	+380931155623	\N	\N	1	f
3	Yurii	patriot.777.pizza78@gmail.com	$2y$10$ezv3dVhkmLzbCl4Ib6MnsuJcyVkkR7oIG.C/5WyMEiEUAN9Qk72Ky	R5yqF6aiPzbLznDBQyJ53kp1cFHWDoYPlXPMPIK4sS8SWI8C4SATtSzstnWO	2017-11-21 22:41:09	2017-11-21 22:41:09	Klymenko	\N	224	+380734202216	\N	\N	1	f
5	MoterataBab	moterata@yandex.ru	$2y$10$GcHY265ZjgUbteic5C1wmOZxlANHaVLKv6BnaGARrJ9qTw7TzyECy	\N	2017-12-09 04:17:03	2017-12-09 04:17:03	MoterataBabZH	\N	179	83747586216	\N	\N	1	f
6	TarabamBab	tvilokov@yandex.ru	$2y$10$Y3gLxOLmC7yihR24BlE64.dlwh17Kjd/MJ/DBM9k/vaEV9Y9WYPyG	\N	2017-12-10 00:14:20	2017-12-10 00:14:20	TarabamBabLX	\N	179	87444782242	\N	\N	1	f
7	Наталья	natalya.okhota07@gmail.com	$2y$10$/Zo2aMYc01CT2kTfCU8.zuevN5MH/31IejCaeT..jKJgQEQSg9tam	\N	2017-12-15 15:03:57	2017-12-15 15:03:57	Охота	\N	179	+79667551637	\N	\N	1	f
8	Admionistrator	admin@admin.com	$2y$10$T7aP/pg1w/fnHIQDflHB1OrKcXbBv/Yaj981kekaKZ5kR9kjNhVWG	4pblzVL8zYIhbwuXYScHdRzo0KiD2EkfED99T5bP2fodCG79HXfc42lK0z38	2017-12-25 18:07:43	2017-12-25 18:07:43	Admin	\N	16	1111111	\N	\N	1	f
9	Alex	mybook@ua.fm	$2y$10$KWZ/0siHPsDoNEyV3cwBeeLAWBLK2G4LA03mSloJ5Il6VOA1uStJi	\N	2017-12-26 16:42:08	2017-12-26 16:42:08	Mamal	\N	224	+380671000163	\N	\N	1	f
10	Иваноа	dmitriy.thorzhevskii@gmail.com	$2y$10$FNr.QbS0.G9g.h7BzFwleOfr..esx3Rr1vMMIy6kCkl9UwRbHxw6C	\N	2017-12-26 16:43:10	2017-12-26 16:43:10	dsfds	\N	224	+380502753289	\N	\N	1	f
11	Вера	veravorzheva@gmail.com	$2y$10$A/Td9SvwEdCF5Cd4BmGI/uTg5OrN1LAURGw1Ezo0Uten24.RWGoPS	40V0g44ulVxPeersnGKFJtSuQG4sa1QEEEiU0mqw8h9J4CdwrkukYAfRZjLI	2017-12-27 12:17:38	2017-12-27 12:20:27	Воржева	Николаевна	224	(050 ) 546-63-06	\N	\N	1	f
12	Vladimir	6063727@gmail.com	$2y$10$czYWi8WOdsMF3Q6GV0kG9ujDC/VfNhHXuRo5WAE2ZlHz/KWT1XPN.	\N	2017-12-27 12:44:47	2017-12-27 12:44:47	Dovgal	\N	224	+380950964121	\N	\N	1	f
4	Алекс	traal@ukr.net	$2y$10$EsE/LNZM63Zoi/HbKicC6ezKjWHF2P4GCI1wK2naF/s8Ay.Ay12v.	ss5MCkL3V5v3s73kYWqPVJX9rIMIAPB33hycZ5YCzYvYDCixMwR48gpQq9TC	2017-12-07 18:01:10	2017-12-07 18:01:10	Югов	\N	1	12344567	\N	\N	1	f
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 12, true);


--
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: country_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY country
    ADD CONSTRAINT country_pkey PRIMARY KEY (id);


--
-- Name: history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY history
    ADD CONSTRAINT history_pkey PRIMARY KEY (id);


--
-- Name: invests_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY invests
    ADD CONSTRAINT invests_pkey PRIMARY KEY (id);


--
-- Name: migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: package_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY package
    ADD CONSTRAINT package_pkey PRIMARY KEY (id);


--
-- Name: package_term_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY package_term
    ADD CONSTRAINT package_term_pkey PRIMARY KEY (id);


--
-- Name: pages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pages
    ADD CONSTRAINT pages_pkey PRIMARY KEY (id);


--
-- Name: payouts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY payouts
    ADD CONSTRAINT payouts_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- Name: categories_parent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES categories(id) ON DELETE CASCADE;


--
-- Name: history_order_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY history
    ADD CONSTRAINT history_order_id_foreign FOREIGN KEY (order_id) REFERENCES orders(id) ON DELETE CASCADE;


--
-- Name: history_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY history
    ADD CONSTRAINT history_user_id_foreign FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: invests_package_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invests
    ADD CONSTRAINT invests_package_id_foreign FOREIGN KEY (package_id) REFERENCES package(id) ON DELETE CASCADE;


--
-- Name: invests_term_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invests
    ADD CONSTRAINT invests_term_id_foreign FOREIGN KEY (term_id) REFERENCES package_term(id) ON DELETE CASCADE;


--
-- Name: invests_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invests
    ADD CONSTRAINT invests_user_id_foreign FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: orders_product_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_product_id_foreign FOREIGN KEY (product_id) REFERENCES products(id) ON DELETE CASCADE;


--
-- Name: orders_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_user_id_foreign FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: package_term_package_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY package_term
    ADD CONSTRAINT package_term_package_id_foreign FOREIGN KEY (package_id) REFERENCES package(id) ON DELETE CASCADE;


--
-- Name: payouts_package_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY payouts
    ADD CONSTRAINT payouts_package_id_foreign FOREIGN KEY (package_id) REFERENCES package(id) ON DELETE CASCADE;


--
-- Name: payouts_term_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY payouts
    ADD CONSTRAINT payouts_term_id_foreign FOREIGN KEY (term_id) REFERENCES package_term(id) ON DELETE CASCADE;


--
-- Name: payouts_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY payouts
    ADD CONSTRAINT payouts_user_id_foreign FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: products_category_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_category_id_foreign FOREIGN KEY (category_id) REFERENCES categories(id) ON DELETE CASCADE;


--
-- Name: role_user_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role_user
    ADD CONSTRAINT role_user_role_id_foreign FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE;


--
-- Name: role_user_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role_user
    ADD CONSTRAINT role_user_user_id_foreign FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: users_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_country_id_foreign FOREIGN KEY (country_id) REFERENCES country(id) ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

