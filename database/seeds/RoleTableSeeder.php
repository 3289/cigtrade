<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::create(['id' => 1, 'name' => 'Administrators']);
        \App\Role::create(['id' => 2, 'name' => 'Moderator']);
        \App\Role::create(['id' => 4, 'name' => 'User']);
    }
}
