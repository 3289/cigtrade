<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = \App\User::where('name', '=', 'Admin')->first();
        $role = \App\Role::where('name', '=', 'Administrators')->first();

        DB::table('role_user')->insert([
            'role_id' => $role->id,
            'user_id' => $admin->id
        ]);
    }
}
