<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategoryEnRu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_en', function (Blueprint $table) {
            $table->integer('id')->references('id')->on('categories')->onDelete('cascade');
            $table->string('name');
        });

        Schema::create('categories_ru', function (Blueprint $table) {
            $table->integer('id')->references('id')->on('categories')->onDelete('cascade');
            $table->string('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories_en');
        Schema::drop('categories_ru');
    }
}
