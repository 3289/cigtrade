<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CREATEPACKAGETERM extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_term', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id');
            $table->foreign('package_id')->references('id')->on('package')->onDelete('cascade');
            $table->integer('term');
            $table->integer('percents');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_term');
    }
}
