<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ADDPROFILE extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('surname');
            $table->string('middle_name')->nullable();
            $table->integer('country_id');
            $table->foreign('country_id')->references('id')->on('country')->onDelete('cascade');
            $table->string('phone');
            $table->string('viber')->nullable();
            $table->string('skype')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('surname');
            $table->dropColumn('middle_name');
            $table->dropColumn('country');
            $table->dropColumn('phone');
            $table->dropColumn('viber');
            $table->dropColumn('skype');
        });
    }
}
