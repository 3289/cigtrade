<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditInvest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invests', function (Blueprint $table) {
            $table->integer('package_id');
            $table->foreign('package_id')->references('id')->on('package')->onDelete('cascade');
            $table->integer('term_id');
            $table->foreign('term_id')->references('id')->on('package_term')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invests', function (Blueprint $table) {
            $table->dropColumn('package_id');
            $table->dropColumn('term_id');
        });
    }
}
