;(function($) {
    
    function Filter(options) {

        this.config = null;

        this.isOpen = null;

        this.input = null;

        this.parent = null;

        this.url = null;

        this.locale = null;

        this.instanceKey = null;

        this.rawData = null;

        this.onSelect = null;

        this.init(options);

    }

    Filter.prototype.init = function(options) {
        this.config = options;

        this.isOpen = false;

        this.instanceKey = Math.random().toString(36).substr(2, 10);

        this.locale = this.config.locale ? '/' + this.config.locale : '';

        this.url = this.config.url;

        this.input = $(this.config.selector);

        this.parent = this.input.parent();

        this.filter = this.createFilterInput();

        this.filter.on('input', null, this, this.inputHandler);

        $(document).on('click', null, this, function (e) {
            var me = e.data;
            if (e.target.className != 'predication' && me.isOpen) me.hidePredictions();
        });

    };

    Filter.prototype.createFilterInput = function() {
        var name = this.input.attr('id');

        var filter = $('<input type="text">')
            .attr('id', name + this.instanceKey)
            .attr('placeholder', this.input.attr('placeholder') || 'Start type here...')
            .attr('class', this.config.classList || 'form-control')
            .attr('autocomplete', 'off')
            .val(this.input.data('default') || '');

        if (this.config.formName) filter.attr('name', this.config.formName);

        this.input.before(filter);

        return filter;
    };

    Filter.prototype.inputHandler = function(e) {
        var me = e.data;
        me.predicationsBox = $('<div class="predications">').addClass(me.instanceKey);

        var value = me.filter.val();

        if (value.length >= 3)
            me.getPredictions(value);
        else if (me.isOpen)
            me.hidePredictions();
    };

    Filter.prototype.getPredictions = function(value) {
        var me = this;

        $.ajax({
            method: 'POST',
            url: this.locale + this.url,
            data: {
                filter: value,
                limit: this.config.limit || 5,
                _token: this.config.token
            },
            success: function (resp) {
                me.rawData = resp.data;
                me.renderPredictions(resp.data, resp.status);
            },
            error: function (resp) {
                $(document.body).html(resp.responseText);
            }
        });
    };

    Filter.prototype.renderPredictions = function(data, status) {
        var me = this;

        if (status == 'OK') {
            me.hidePredictions();
            me.predicationsBox = $('<div class="predications">').addClass(me.instanceKey);
            if (data.length) {
                data.forEach(function (item, index, array) {
                    $('<div class="predication">')
                        .attr('index', index)
                        .attr('data-id', item[me.config.valueField || 'id'])
                        .html(item[me.config.displayField || 'name'])
                        .addClass(me.instanceKey)
                        .appendTo(me.predicationsBox);
                });
            } else {
                $('<div class="predication">')
                    .attr('data-id', null)
                    .html('No results were found')
                    .addClass(me.instanceKey)
                    .appendTo(this.predicationsBox);
            }

            if (this.isOpen) this.hidePredictions();

            this.predicationsBox.css({
                top: this.filter.offset().top + 33,
                left: this.filter.offset().left
            }).appendTo('body');

            this.isOpen = true;

            $('.predication.' + this.instanceKey).on('click', null, me, me.clickHandler);
        }
    };

    Filter.prototype.clickHandler = function(e) {
        var me = e.data;

        if (!this.dataset.id) {
            me.filter.val('');
            me.input.val(null);
            me.hidePredictions();

            return;
        }

        me.input.attr('value', this.dataset.id);
        me.filter.val(this.textContent);

        if (typeof me.onSelect === 'function') {
            me.onSelect(me.rawData, this.dataset.id);
        }

        me.hidePredictions();
    };

    Filter.prototype.hidePredictions = function() {
        $('.predications.' + this.instanceKey).remove();

        this.isOpen = false;
    };

    Filter.prototype.clear = function() {
        this.filter.val('');
    };

    window.Filter = Filter;

})(jQuery);