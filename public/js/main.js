'use strict';

$(document).ready(function () {
    var mediaPoupop = document.querySelector('.media-content');
    var closePoupop = document.querySelector('.media-content .close');
    var blockedPoupop = document.querySelector('.blocked-poupop');
    var mediaPoupopBtn = document.querySelector('.poupop-media-btn a');

    $(mediaPoupop).hide();
    $(blockedPoupop).hide();

    // poupop open function
    $(mediaPoupopBtn).click(function (e) {
        e.preventDefault();
        $(blockedPoupop).fadeIn(500);
        $(mediaPoupop).fadeIn(500);
        $('header, footer, section').css({ filter: 'blur(2px)' });
    });

    // poupop close function
    $(closePoupop).click(function (e) {
        e.preventDefault();
        $(blockedPoupop).fadeOut(500);
        $(mediaPoupop).fadeOut(500);
        $('header, footer, section').css({ filter: 'blur(0px)' }).removeAttr('style');
        if ($('.uppod-control_pause').is(':visible')) {
            $('.uppod-control_pause').click();
        }
    });
    // poupop close function
    $(blockedPoupop).click(function (e) {
        e.preventDefault();
        $(blockedPoupop).fadeOut(500);
        $(mediaPoupop).fadeOut(500);
        $('header, footer, section').css({ filter: 'blur(0px)' }).removeAttr('style');
        if ($('.uppod-control_pause').is(':visible')) {
            $('.uppod-control_pause').click();
        }
    });

    if ($('.main-menu').is(':visible')) {
        $('#menu').hide();
    };

    //    $('.scroll-ancher').click(function () {
    //        $('#menu').slideUp(1000);
    //    });

    //burger-menu
    $('.main-menu').click(function () {
        if ($(this).hasClass('active-menu')) {
            $(this).removeClass('active-menu').addClass('not-active-menu');
            $('.bar2').css('display', 'block');
            $('.bar1').css({
                transform: 'rotate(0deg)',
                position: 'relative',
                top: '0px'
            });
            $('.bar3').css({
                transform: 'rotate(-0deg)',
                position: 'relative',
                top: '0px'
            });
            $('#menu').slideUp(500);
        } else {
            $(this).removeClass('not-active-menu').addClass('active-menu');
            $('.bar2').css('display', 'none');
            $('.bar1').css({
                transform: 'rotate(45deg)',
                position: 'relative',
                top: '7px'
            });
            $('.bar3').css({
                transform: 'rotate(-45deg)',
                position: 'relative',
                top: '-5px'
            });
            $('#menu').slideDown(500);
        }
    });

    $('.scroll-ancher').click(function () {
        if ($('.main-menu').hasClass('active-menu')) {
            $('.main-menu').click();
        }
    });
    
    $('.open-hidden').click(function () {
        $('p.hidden').slideToggle(500);
        $(this).remove();
    });
    
    // pop100
    /*$('.pop1').click(function () {
        $('.pop100').fadeIn();
        $('.pop100block').fadeIn();
    });
    $('.pop100block').click(function () {
        $('.pop100').fadeOut();
        $('.pop100block').fadeOut();
    });
    // pop1000
    $('.pop2').click(function () {
        $('.pop1000').fadeIn();
        $('.pop1000block').fadeIn();
    });
    $('.pop1000block').click(function () {
        $('.pop1000').fadeOut();
        $('.pop1000block').fadeOut();
    });
    // pop5000
    $('.pop3').click(function () {
        $('.pop5000').fadeIn();
        $('.pop5000block').fadeIn();
    });
    $('.pop5000block').click(function () {
        $('.pop5000').fadeOut();
        $('.pop5000block').fadeOut();
    });*/
});
//# sourceMappingURL=../js/main.js.map
