var localities = ['ru', 'en'];
var fields = ['name'];

$(document).ready(function() {
    $(".lang_selector").click(function() {
        localities.forEach(function(item)
        {
            $("#lang_" + item).css('backgroundColor', 'transparent');
            fields.forEach(function (field) {
                $("#" + field + "_" + item).css('display', 'none');
            });
        });
        $("#" + this.id).css('backgroundColor', '#00c0ef' );
        var parts = this.id.split('_');
        fields.forEach(function (field) {
            $("#" + field + "_" + parts[1]).css('display', 'block');
        });
    });
});