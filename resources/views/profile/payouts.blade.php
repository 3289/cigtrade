@extends('layouts.empty')

@section('content')

    <div class="profile-content">

        <div class="box-body">

            <h2>Вклады</h2>
            <table class="table table-bordered" style="margin-top: 20px;">
                <thead>
                <tr>
                    <th style="width: 20%;">Дата начала</th>
                    <th style="width: 20%;">Сумма</th>
                    <th style="width: 20%;">Проценты</th>
                    <th style="width: 20%;">Всего заработано</th>
                    <th style="width: 20%;">Дней осталось</th>
                </tr>
                </thead>
                <tbody>
                @foreach($user->invests as $invest)
                    <tr>
                        <td>{{$invest->start ? $invest->start->format('Y-m-d H:i:s') : "-"}}</td>
                        <td>$ {{$invest->amount}}</td>
                        <td>{{$invest->percents}} %</td>
                        <td>
                            $ {{number_format($invest->earned(), 2, '.', '')}}
                        </td>
                        <td style="text-align: center;">
                            @if($invest->left() < 0 || !$invest->monthly())
                                {{$invest->left()}}
                            @else
                                <form method="POST" action="{{action('PayoutController@order_invest')}}">
                                    <input type="hidden" name="id" value="{{$invest->id}}">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <button type="submit" value="" class="fa fa-credit-card"
                                            style="background:none!important;border:none;color: #3c8dbc;"/>
                                </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <h2>Выплаты</h2>
            <table class="table table-bordered" style="margin-top: 20px;">
                <thead>
                <tr>
                    <th style="width: 493px;">Дата заказа</th>
                    <th style="width: 493px;">Сумма</th>
                    <th style="width: 493px;">Статус</th>
                </tr>
                </thead>
                <tbody>
                @foreach($user->payouts as $payout)
                    <tr>
                        <td>{{$payout->created_at->format('Y-m-d H:i:s')}}</td>
                        @if($payout->type=='referal')
                            <td>$ {{$payout->amount*0.1}}</td>
                            @else
                            <td>$ {{$payout->amount}}</td>
                            @endif
                        <td>@if($payout->done == 1)
                                Выполнено
                            @else
                                В обработке...
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
