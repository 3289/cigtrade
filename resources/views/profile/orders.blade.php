@extends('layouts.empty')

@section('content')

                <div class="profile-content">
                    <h2>Список покупок</h2>
                    <div class="box-body">
                        <div class="row">

                            <table class="table table-bordered" style="margin-top: 20px;">
                                <thead>
                                <tr>
                                    <th style="width: 493px;">Дата и время</th>
                                    <th style="width: 493px;">Продукт</th>
                                    <th style="width: 493px;">Сумма</th>
                                    <th style="width: 493px;">Статус заказа</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($user->orders as $order)
                                        <tr>
                                            <td>{{$order->created_at->format('d.m.Y')}}</td>
                                            <td>{{$order->product->name}}</td>
                                            <td>{{$order->amount}}</td>
                                            <td>{{$order->state}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

@endsection
