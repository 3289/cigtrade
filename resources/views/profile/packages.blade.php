@extends('layouts.empty')

@section('content')

    <div class="profile-content">
        <h2>Инвестиции под Трейдинг и Майнинг</h2>
        @foreach($packages as $package)
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="row planBox plus text-center">
                    <div class="col-md-12 section1">
                        <p>{{$package->name}}</p>
                        <h1><sup>$</sup>{{$package->amount_min}}<span>-</span><sup>$</sup>{{$package->amount_max}}</h1>
                    </div>
                    <div class="col-md-12 section2">
                        @foreach($package->terms as $term)
                            <p>{{$term->term}} days - {{$term->percents}} %</p><br>
                        @endforeach
                    </div>
                    <div class="col-md-12 section3">
                        <a href="/profile/invest/{{$package->id}}">
                            <button class="btn btn-block saveBtn">Invest</button>
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection
