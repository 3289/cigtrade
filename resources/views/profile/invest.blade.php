@extends('layouts.empty')

@section('content')

                <div class="profile-content">
                    <h2>{{$package->name}}</h2>
                    <p>
                        {!! $package->description !!}
                    </p>

                    <form id="invest_form_id" class="form-horizontal" role="form" action="{{action('ProfileController@make_invest')}}" method="post">

                        <div class="form-group">
                            <label for="inputAmount" class="col-sm-3">Сумма инвестиции</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="amount" name="amount" placeholder="{{$package->amount_min}}-{{$package->amount_max}}">
                            </div>
                        </div>

                        @foreach($package->terms as $term)
                            <div class="radio">
                                <label>
                                    <input type="radio" checked value="{{$term->id}}" name="termId"/>{{$term->term}} days - {{$term->percents}} %
                                </label>
                            </div>
                        @endforeach

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    </form>

                    <br/>
                    <label>Выберите способ оплаты:</label>
                    <div class="row">
                        <div class="col-lg-2">
                            <button type="button" onclick="make_invest('cryptonator')" class="btn btn-primary">Криптовалюты</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" onclick="make_invest('cryptonator')" class="btn btn-primary">Perfect Money</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" onclick="make_invest('cryptonator')" class="btn btn-primary">Liq Pay</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" onclick="make_invest('cryptonator')" class="btn btn-primary">BitCoin</button>
                        </div>
                    </div>

                    <form id="cryptonator" method="GET" action="https://api.cryptonator.com/api/merchant/v1/startpayment">
                        <input type="hidden" name="merchant_id" value="74fe54295db2c52874c3196cc0e42d97">
                        <input type="hidden" name="item_name" value="{{$package->name}} {{$term->term}} days - {{$term->percents}} %">
                        <input type="hidden" name="order_id" value="">
                        <input type="hidden" name="invoice_currency" value="usd">
                        <input type="hidden" name="invoice_amount" value="" data-type="number">
                        <input type="hidden" name="language" value="en">
                        <input type="hidden" name="success_url" value="http://cigrade.com/invests">
                        <input type="hidden" name="failed_url" value="http://cigtrade.com/cryptonator/fail">
                    </form>
                </div>

    <script type="application/javascript">
        function make_invest(payment)
        {
            switch (payment) {
                case 'nixmoney': {
                    $.ajax({
                        type: "POST",
                        url: "{{action('ProfileController@make_invest')}}",
                        data: {
                            _token: "{{csrf_token()}}",
                            termId: $('input[name=termId]:checked', '#invest_form_id').val(),
                            amount: $('#amount').val()
                        },
                        success: function (data) {
                            $('input[name=PAYMENT_ID]', '#nixmoney').val(data.id);
                            $('input[name=PAYMENT_AMOUNT]', '#nixmoney').val(data.amount);
                            $('#nixmoney').submit();
                        }
                    });
                    break;
                }
                case 'cryptonator': {
                    $.ajax({
                        type: "POST",
                        url: "{{action('ProfileController@make_invest')}}",
                        data: {
                            _token: "{{csrf_token()}}",
                            termId: $('input[name=termId]:checked', '#invest_form_id').val(),
                            amount: $('#amount').val()
                        },
                        success: function (data) {
                            $('input[name=order_id]', '#cryptonator').val(data.id);
                            $('input[name=invoice_amount]', '#cryptonator').val(data.amount);
                            $('#cryptonator').submit();
                        }
                    });
                    break;
                }
            }
        }
    </script>

@endsection
