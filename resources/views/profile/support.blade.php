@extends('layouts.empty')

@section('content')

    <div class="profile-content">
        <h2>Служба Поддержки</h2>
        <div class="box-body">

            <div class="row">
                <div class="box-body">
                    <form id="ref_payout" action="{{action('ProfileController@send')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="amount">Тема</label>
                            <select name="theme" class="form-control">
                                <option value="payout">Запрос на вывод</option>
                                <option value="consult">Запрос на консультацию</option>
                                <option value="technical">Технические вопросы</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="amount">Ваш вопрос</label>
                            <textarea id="requisites" name="question" class="form-control" rows="10" cols="80" required></textarea>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <button type="submit" form="ref_payout" class="btn btn-block">Отправить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
