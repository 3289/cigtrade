@extends('layouts.empty')

@section('content')

    <div class="profile-content">
        <h2>Мой профиль</h2>
        <form class="form-horizontal" method="POST"
              action="{{ action('ProfileController@update', ['id' => Auth::getUser()->id]) }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">Имя</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ Auth::getUser()->name }}"
                           required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                <label for="surname" class="col-md-4 control-label">Фамилия</label>

                <div class="col-md-6">
                    <input id="surname" type="text" class="form-control" name="surname"
                           value="{{ Auth::getUser()->surname }}" required autofocus>

                    @if ($errors->has('surname'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>


            <div class="form-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                <label for="middle_name" class="col-md-4 control-label">Отчество</label>

                <div class="col-md-6">
                    <input id="middle_name" class="form-control" name="middle_name"
                           value="{{ Auth::getUser()->middle_name }}">

                    @if ($errors->has('middle_name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <label for="phone" class="col-md-4 control-label">Телефон</label>

                <div class="col-md-6">
                    <input id="phone" class="form-control" placeholder="+3_(___) ___-__-__" type="tel"
                           name="phone" value="{{ Auth::getUser()->phone }}"
                           required autofocus>
                    @if ($errors->has('phone'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="country" class="col-md-4 control-label">Страна</label>

                <div class="col-md-6">
                    {!! Form::select('country_id', $countries, Auth::getUser()->country_id,
                        ['class' => 'form-control', 'id' => 'country_id']) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('skype') ? ' has-error' : '' }}">
                <label for="skype" class="col-md-4 control-label">Skype</label>

                <div class="col-md-6">
                    <input id="skype" class="form-control" name="skype" value="{{ Auth::getUser()->skype }}">

                    @if ($errors->has('middle_name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('skype') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('viber') ? ' has-error' : '' }}">
                <label for="viber" class="col-md-4 control-label">Viber</label>

                <div class="col-md-6">
                    <input id="viber" class="form-control" name="viber" value="{{ Auth::getUser()->viber }}">

                    @if ($errors->has('viber'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('viber') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('telegram') ? ' has-error' : '' }}">
                <label for="telegram" class="col-md-4 control-label">Telegram</label>

                <div class="col-md-6">
                    <input id="telegram" class="form-control" name="telegram" value="{{ Auth::getUser()->telegram }}">

                    @if ($errors->has('telegram'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('telegram') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">Пароль</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-md-4 control-label">Пароль (подтверждение)</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Сохранить
                    </button>
                </div>
            </div>
        </form>
    </div>

@endsection
