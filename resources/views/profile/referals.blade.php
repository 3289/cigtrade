@extends('layouts.empty')

@section('content')

    <div class="profile-content">
        <h2>Реферальная программа</h2>
        <div class="box-body">

            <div class="row">
                <div class="form-group">
                    <label for="ref_url" class="col-md-2 control-label">Ваша ссылка</label>
                    <div class="col-md-10">
                        <input type="text" id="ref_url" class="form-control" readonly
                               value="http://cigtrade.com/ref/379{{$user->id}}">
                    </div>
                </div>
            </div>
            @if($user->getReferals()->isNotEmpty())
                <table class="table table-bordered" style="margin-top: 20px;">
                    <thead>
                    <tr>
                        <th style="width: 473px;">Имя</th>
                        <th style="width: 473px;">Номер ID</th>
                        <th style="width: 473px;">Сумма вклада</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->getReferals() as $referal)
                        <tr>
                            <td>{{$referal->name}}</td>
                            <td>379{{$referal->user_id}}</td>
                            <td>$ {{$referal->amount * 0.1}}</td>

                        </tr>
                    @endforeach
                    </tbody>

                </table>
                Мои реф. бонусы:<br>
                Общая сумма: $ {{array_sum($user->getReferals()->pluck('amount')->toArray()) * 0.1}}<br>
                На сегодн. дату: {{date('d.m.Y')}}

                <form id="ref_payout" action="{{action('PayoutController@orderReferal')}}"
                      method="post">
                    {{ csrf_field() }}
                    <br>
                    <button type="submit" value="" class="fa fa-credit-card"
                            style="background:none!important;border:none;color: #3c8dbc;">Вывод
                    </button>
                </form>
                @else
                <h2>Нет доступных средств для вывода</h2>
            @endif
        </div>
    </div>

@endsection
