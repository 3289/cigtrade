@extends('layouts.empty')

@section('content')

    <div class="profile-content">
        <h2>Мои инвестиции</h2>
        <div class="box-body">
            <div class="row">
                @foreach($packages as $package)
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="row planBox plus text-center">
                            <div class="col-md-12 section1">
                                <p>{{$package->name}}</p>
                                <h1><sup>$</sup>{{$package->amount_min}}-<sup>$</sup>{{$package->amount_max}}</h1>
                            </div>
                            <div class="col-md-12 section2">
                                @foreach($package->terms as $term)
                                    <p>{{$term->term}} дней - {{$term->percents}} %</p><br>
                                @endforeach
                            </div>
                            <div class="col-md-12 section3">
                                <a href="/profile/invest/{{$package->id}}">
                                    <button class="btn btn-block saveBtn">Заказать</button>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>


        </div>
    </div>

@endsection
