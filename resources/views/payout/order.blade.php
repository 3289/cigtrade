@extends('layouts.empty')

@section('content')

    <div class="profile-content">
        <h2>Заказ выплаты</h2>
        <div class="box-body">
            <div class="row">
                <div class="box-body">
                    <form id="ref_payout" action="{{action('PayoutController@makeReferal')}}" method="post">
                        <input type="hidden" name="id" value="{{$id}}">
                        <input type="hidden" name="type" value="{{$type}}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <input readonly type="text" maxlength="255" name="amount" class="form-control"
                                   value="{{$amount}}">
                        </div>
                        <h5>Напишите свои платежные реквизиты или куда бы вы хотели, чтоб вам перевели ваши реферальные
                            бонусы.</h5>
                        <div class="form-group">
                            <label for="amount">Requisites</label>
                            <textarea id="requisites" name="requisites" class="form-control" rows="10"
                                      cols="80"></textarea>
                        </div>
                        <button onclick="$('ref_payout').submit()" class="btn btn-block">Order</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
