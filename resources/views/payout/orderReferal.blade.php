@extends('layouts.empty')

@section('content')
    <div class="profile-content">
        <h2>Заказ выплаты реферальных на сумму {{array_sum($user->getReferals()->pluck('amount')->toArray()) * 0.1}} $</h2>

        <div class="box-body">
            <div class="row">
                <div class="box-body">
                    <form id="ref_payout" action="{{action('PayoutController@makeReferal')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Тема</label>
                            <input readonly type="text" maxlength="255" class="form-control"
                                   value="Заказ выплаты реферальных  для пользователя {{$user->name}}">
                        </div>
                        <h5>Напишите свои платежные реквизиты или куда бы вы хотели, чтоб вам перевели ваши реферальные
                            бонусы.</h5>
                        <div class="form-group">
                            <label for="amount">Реквизиты</label>
                            <textarea id="requisites" name="requisites" class="form-control" rows="10"
                                      cols="80" required></textarea>
                        </div>
                        <button type="submit" form="ref_payout" class="btn btn-block">Заказать выплату</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
