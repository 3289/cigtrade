@extends('layouts.empty')

@section('content')

    <div class="profile-content">
        <div class="row">
            <h2>{{$product->name}} - $ {{$product->price}}</h2>
            <p>{!! $product->description !!}</p>

            <div class="col-md-2">
                <form action="{{action('ProductsController@buy')}}" method="post">
                    <input type="hidden" name="id" value="{{$product->id}}">
                    {{ csrf_field() }}
                    <button class="btn btn-block">Buy</button>
                </form>
            </div>

        </div>
    </div>

@endsection
