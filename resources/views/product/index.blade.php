@extends('layouts.empty')

@section('content')

    <div class="profile-content">
        <h2>Вычислительное оборудование</h2>
        @foreach($products as $product)
            <div class="col-md-12">
                <div class="row" style="padding-top: 10px;">
                    <div class="col-md-8"><a href="{{action('ProductsController@item', ['id' => $product->id])}}">{{$product->name}}</a></div>
                    <div class="col-md-2">$&nbsp;{{$product->price}}</div>
                    <div class="col-md-2">
                        <form action="{{action('ProductsController@buy')}}" method="post">
                            <input type="hidden" name="id" value="{{$product->id}}">
                            {{ csrf_field() }}
                            <button class="btn btn-block">Купить</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach

        <div style="clear:both;"></div>

        <p>ИННОВАЦИИ И ТЕХНОЛОГИИ (В разработке)</p>

        <p>КРАУДИНВЕСТИНГ И СТАРТАПЫ (В разработке)</p>
    </div>

@endsection
