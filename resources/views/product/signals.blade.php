@extends('layouts.empty')

@section('content')

    <div class="profile-content">
        <h2>Аналитические Сигналы по Торговле на Бирже</h2>
        @foreach($products as $product)
            <div class="col-md-12">
                <div class="row" style="padding-top: 10px;">
                    <div class="col-md-8">{{$product->name}}</div>
                    <div class="col-md-2">$&nbsp;{{$product->price}}</div>
                    <div class="col-md-2">
                        <form action="{{action('ProductsController@buy')}}" method="post">
                            <input type="hidden" name="id" value="{{$product->id}}">
                            {{ csrf_field() }}
                            <button class="btn btn-block">Купить</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection
