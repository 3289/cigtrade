<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="theme-color" content="#2ea3f2">
    <meta name="msapplication-navbutton-color" content="#2ea3f2">
    <meta name="apple-mobile-web-app-status-bar-style" content="#2ea3f2">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="revisit-after" content="2 days">
    <meta name="robots" content="index,all">
    <title>СryptoInvestGroup | Добро пожаловать в Будущее!</title>
    <link rel="stylesheet" href="{{asset('css/libs.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="icon" href="{{asset('img/icon/cropped-192x192.png')}}" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="{{asset('img/icon/cropped-180x180.png')}}">
    <meta name="msapplication-TileImage" content="{{asset('img/icon/cropped-270x270.png')}}">

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{asset('css/profile.css')}}">
    <link rel="stylesheet" href="{{asset('css/plan.css')}}">




</head>

<body style="background: url('{{asset('/img/header-banner.jpg')}}') no-repeat fixed 0/cover;">

<div class="container">
    <div class="row profile">
        <div class="col-lg-12">
                <div class="profile-usertitle-name">
                    <div class="logo" style="float: left;">
                        <a href="http://cigtrade.local/ru"><img src="/img/logoheader.png" alt="logo"></a>
                    </div>
                    Добро пожаловать, {{$user->name}}! (ID: 379{{$user->id}}) @if ($user->hasRole(['Administrators'])) <a href="/admin">Админка</a> @endif
                </div>
        </div>
        <div class="col-md-3">
            <div class="profile-sidebar">
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li>
                            <a href="/home">
                                <i class="fa fa-btc"></i>
                                Мой Кабинет </a>
                        </li>
                        <li>
                            <a href="/profile">
                                <i class="fa fa-user"></i>
                                Мой Профиль </a>
                        </li>
                        <li>
                            <a href="/payouts">
                                <i class="fa fa-user"></i>
                                Мои Вклады и Выплаты </a>
                        </li>
                        <li>
                            <a href="/invests">
                                <i class="fa fa-user"></i>
                                Мои Инвестиции </a>
                        </li>
                        <li>
                            <a href="/history/orders">
                                <i class="fa fa-shopping-basket"></i>
                                Мои Покупки </a>
                        </li>
                        <li>
                            <a href="/referals">
                                <i class="fa fa-users"></i>
                                Моя Реф. Программа </a>
                        </li>
                        <li>
                            <a href="{{action('ProfileController@support')}}">
                                <i class="fa fa-users"></i>
                                Служба Поддержки </a>
                        </li>
                        <li>
                            <a href="{{action("Auth\LoginController@logout")}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i>
                                Выход </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
        </div>
        <div class="col-md-9">
            @yield('content')
        </div>
    </div>
</div>

</body>
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script src="{{asset('js/telephoneChecker.js')}}"></script>
</html>