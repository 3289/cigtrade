<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="theme-color" content="#2ea3f2">
    <meta name="msapplication-navbutton-color" content="#2ea3f2">
    <meta name="apple-mobile-web-app-status-bar-style" content="#2ea3f2">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="revisit-after" content="2 days">
    <meta name="robots" content="index,all">
    <title>СryptoInvestGroup | Добро пожаловать в Будущее!</title>
    <link rel="stylesheet" href="{{asset('css/libs.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="icon" href="{{asset('img/icon/cropped-192x192.png')}}" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="{{asset('img/icon/cropped-180x180.png')}}">
    <meta name="msapplication-TileImage" content="{{asset('img/icon/cropped-270x270.png')}}">

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{asset('css/profile.css')}}">
</head>

<body>
<header>
    <div class="nav">
        <div class="container">
            <div class="logo">
                <a href="{{ action("HomeController@index") }}"><img src="/img/logoheader.png" alt="logo"></a>
            </div>

            <?php
            $locale = App::getLocale();
            $url = Request::instance()->server->get('REQUEST_URI');
            ?>

            <div class="lang-box">
                @if(Auth::guest())
                    <a class="enter" href="{{ action("Auth\LoginController@login") }}">Войти</a>
                    <a class="lang-link lang-active" href="{{str_replace("/".$locale, '/ru', $url)}}">Рус</a>
                    <a class="lang-link" href="{{str_replace("/".$locale, '/en', $url)}}">Eng</a>
                    <a href="{{ action("Auth\RegisterController@register") }}" class="register">Регистрация</a>
                @else
                    <a class="enter" href="{{ action("ProfileController@home") }}">Профиль</a>
                    <a class="enter" href="{{action("Auth\LoginController@logout")}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Выход </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                @endif
            </div>
            <nav>

                <div class="main-menu not-active-menu">
                    <div class="bar bar1"></div>
                    <div class="bar bar2"></div>
                    <div class="bar bar3"></div>
                </div>

            </nav>
        </div>
    </div>

    <div style="clear: both;"></div>

    @yield('content')
</header>

</body>
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script src="{{asset('js/telephoneChecker.js')}}"></script>
</html>