@extends('admin.dashboard')
@section('content')

    <div class="col-md-6">

        @if(Session::has('message'))
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Massage</h4>
                {{Session::get('message')}}
            </div>
            @endif

                    <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add new role</h3>
                </div><!-- /.box-header -->

                <form method="POST" action="{{action('Admin\RolesController@store')}}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="Title">Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Title">
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>


            </div><!-- /.box-body -->
    </div><!-- /.box -->
@endsection