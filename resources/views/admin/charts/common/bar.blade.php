@extends('admin.dashboard')
@section('content')

    {!! Charts::assets() !!}

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{$title}}</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-lg-2">
                <div class="form-group">
                    <label>Date:</label>
                    <input type="text" class="form-control form-control-1 input-sm from" placeholder="Date" value="{{$month}}/{{$year}}" >
                </div>
                <div class="form-group">
                    <label>Type:</label>
                    <select class="form-control form-control-1 input-sm type">
                        <option {{$type=='bar'?'selected':''}} value="bar">Bar</option>
                        <option {{$type=='line'?'selected':''}} value="line">Line</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-10">
                @yield('chart')
            </div>
        </div>
    </div>


@endsection

@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/demo/MonthPicker.min.css">
<link rel="stylesheet" type="text/css" href="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/test/test.css">
@endpush

@push('js')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="https://rawgit.com/digitalBush/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/demo/MonthPicker.min.js"></script>
<script type="application/javascript">
    $('.from').MonthPicker({
        Button: false,
        OnAfterChooseMonth: function () {
            window.location = '{{$action}}/{{$type}}/' + $(this).val();
        }
    });

    $('.type').change(function() {
        window.location = '{{$action}}/'+ $( this ).val() +'/' + $('.from').val();
    })
</script>
@endpush