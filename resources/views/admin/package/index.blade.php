@extends('admin.dashboard')
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Packages list</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-11">
                    </div>
                    <a href="{{action('Admin\PackagesController@create')}}">
                        <div class="col-sm-1">
                            <button type="submit" class="btn btn-block btn-success">Add</button>
                        </div>
                    </a>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" style="width: 493px;">Title</th>
                                <th tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 40px;"></th>
                                <th tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 40px;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($packages as $package)
                                <?php $formName = "orgRemove".$package->id ?>
                                <tr role="row" class="odd">
                                    <td class="sorting_1">{{$package->name}}</td>
                                    <td style="text-align: center;">
                                        <a href="{{action('Admin\PackagesController@edit',['package'=>$package->id])}}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td style="text-align: center;">
                                        <form method="POST" action="{{action('Admin\PackagesController@destroy',['package'=>$package->id])}}">
                                            <input type="hidden" name="_method" value="delete"/>
                                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                            <button type="submit" value="" class="fa fa-remove" style="background:none!important;border:none;color: #3c8dbc;" />
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(Session::has('message'))
                {{Session::get('message')}}
            @endif
        </div><!-- /.box-body -->
    </div>

    <script>
        var btnArray = document.querySelectorAll('.fa-remove');
        for (var button in btnArray) {
            btnArray[button].addEventListener('click', function(e, opts) {
                var answer = confirm('Вы собираетесь удалить страницу, вы уверены ? Отменить удаление не возможно!');

                if (!answer) { e.preventDefault(); e.stopPropagation(); }
            });
        }
    </script>
@endsection