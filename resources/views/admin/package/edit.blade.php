@extends('admin.dashboard')
@section('content')

    <div class="col-md-6">

        @if(Session::has('message'))
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Massage</h4>
                {{Session::get('message')}}
            </div>
        @endif

        <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактировать пакет</h3>
                </div><!-- /.box-header -->

                <form method="POST" action="{{action('Admin\PackagesController@update', ['package' => $package->id])}}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">Название</label>
                            <input type="text" maxlength="255" name="name" class="form-control" placeholder="Title" value="{{$package->name}}">
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-1">
                                <label>Вклад (от - до)</label>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="amount_min" maxlength="255" class="form-control" placeholder="min" value="{{$package->amount_min}}">
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="amount_max" maxlength="255" class="form-control" placeholder="max" value="{{$package->amount_max}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="editor1">Описание пакета</label>
                            <textarea id="editor1" name="description" rows="10" cols="80">{{$package->description}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="sort_oder">Порядок сортировки</label>
                            <input type="text" maxlength="255" name="sort_order" class="form-control" placeholder="Sort order" value="{{$package->sort_order}}">
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="_method" value="put">
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>


            </div><!-- /.box-body -->
    </div><!-- /.box -->

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Редактировать сроки инвестиций</h3>
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group row">
                    <div class="col-xs-1">
                        <label>Term</label>
                    </div>
                    <div class="col-xs-4">
                        <input type="text" name="term" id="term" maxlength="255" class="form-control" placeholder="Term (days)">
                    </div>
                    <div class="col-xs-4">
                        <input type="text" name="percents" id="percents" maxlength="255" class="form-control" placeholder="Percents (%)">
                    </div>
                    <div class="col-xs-2">
                        <input type="button" id="addTerm" class="btn btn-primary" value="Add Term"/>
                    </div>
                </div>

                <table  class="table table-bordered table-striped dataTable" role="grid">
                    <thead>
                    <tr>
                        <th tabindex="0" rowspan="1" colspan="1"  style="width: 493px;">Дни</th>
                        <th tabindex="0" rowspan="1" colspan="1"  style="width: 493px;">Проценты</th>
                        <th tabindex="0" rowspan="1" colspan="1" style="width: 40px;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($package->terms as $term)
                        <tr role="row" class="odd">
                            <td>{{$term->term}}</td>
                            <td>{{$term->percents}}&nbsp;%</td>
                            <td style="text-align: center;">
                                <button class="fa fa-remove deprive" data-id="{{ $term->id }}" style="background:none!important;border:none;color:#3c8dbc;"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('js')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        CKEDITOR.replace('editor1');
    });

    $('#addTerm').click(function() {
        $.ajax({
            method: 'POST',
            url: '/ajax/term/add',
            data: {
                package_id: '{{$package->id}}',
                term: $('#term').val(),
                percents: $('#percents').val(),
                _token: '{{ csrf_token() }}'
            },
            success: function (resp) {
                document.location += '';
            },
            error: function (resp) {
                $(document.body).html(resp.responseText);
            }
        });
    });

    $('.deprive').click(function() {
        var termId = this.dataset.id;
        var me = this;

        $.ajax({
            method: 'POST',
            url: '/ajax/term/remove',
            data: {
                termId: termId,
                _token: '{{ csrf_token() }}'
            },
            success: function (resp) {
                $(me).parents('tr[role=row]').remove();
            },
            error: function (resp) {
                $(document.body).html(resp.responseText);
            }
        });
    });

</script>
@endpush