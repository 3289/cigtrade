@extends('admin.dashboard')
@section('content')

    <div class="col-md-6">

        @if(Session::has('message'))
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Massage</h4>
                {{Session::get('message')}}
            </div>
            @endif

                    <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавить новый пакет</h3>
                </div><!-- /.box-header -->

                <form method="POST" action="{{action('Admin\PackagesController@store')}}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">Название</label>
                            <input type="text" maxlength="255" name="name" class="form-control" placeholder="Title">
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-1">
                                <label>Вклад (от - до)</label>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="amount_min" maxlength="255" class="form-control" placeholder="min">
                            </div>
                            <div class="col-xs-4">
                                <input type="text" name="amount_max" maxlength="255" class="form-control" placeholder="max">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="editor1">Описание пакета</label>
                            <textarea id="editor1" name="description" rows="10" cols="80"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="sort_oder">Порядок сортировки</label>
                            <input type="text" maxlength="255" name="sort_order" class="form-control" placeholder="Sort order">
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Создать</button>
                    </div>
                </form>


            </div><!-- /.box-body -->
    </div><!-- /.box -->
@endsection

@push('js')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        CKEDITOR.replace('editor1');
    });
</script>
@endpush