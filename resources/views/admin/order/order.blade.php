@extends('admin.dashboard')
@section('content')

    <div class="col-md-6">

        @if(Session::has('message'))
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Massage</h4>
                {{Session::get('message')}}
            </div>
    @endif

    <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Order details</h3>
            </div><!-- /.box-header -->

            <table class="table">
                <tr>
                    <td>ID</td>
                    <td>{{$order->id}}</td>
                </tr>
                <tr>
                    <td>User</td>
                    <td>{{$order->user->name}}</td>
                </tr>
                <tr>
                    <td>Product</td>
                    <td>{{$order->product->name}}</td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td>{{$order->amount}}</td>
                </tr>
                <tr>
                    <td>State</td>
                    <td>{{$order->state}}</td>
                </tr>
                <tr>
                    <td>Comment</td>
                    <td>{{$order->comment}}</td>
                </tr>
                @if($order->state != 'done')
                <tr>
                    <td></td>
                    <td>
                        <form id="ref_payout" action="{{action('Admin\OrdersController@confirm')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$order->id}}">
                            <button type="submit" class="btn btn-primary">Confirm</button>
                        </form>
                    </td>
                </tr>
                @endif
            </table>


        </div><!-- /.box-body -->
    </div><!-- /.box -->

@endsection