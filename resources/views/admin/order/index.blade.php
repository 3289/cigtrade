@extends('admin.dashboard')
@section('content')


    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Orders list</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width: 40px;">ID</th>
                    <th>User</th>
                    <th>Product</th>
                    <th style="width: 100px;">Amount</th>
                    <th style="width: 40px;">Status</th>
                    <th style="width: 200px;">Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($orders as $order)
                    <tr class='clickable-row' data-href='{{action('Admin\OrdersController@item', ['id' => $order->id])}}'>
                        <td>{{$order->id}}</td>
                        <td>{{$order->user->name}}</td>
                        <td>{{$order->product->name}}</td>
                        <td>{{$order->amount}}</td>
                        <td>{{$order->state}}</td>
                        <td>{{($order->created_at)?$order->created_at->format('d.m.Y H:i:s'):""}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @if(Session::has('message'))
            {{Session::get('message')}}
        @endif
    </div><!-- /.box-body -->

@endsection

@push('js')
<script type="application/javascript">
    $(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
</script>
@endpush