@extends('admin.dashboard')
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Products list</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-11">
                    </div>
                    <a href="{{action('Admin\ProductsController@create')}}">
                        <div class="col-sm-1">
                            <button type="submit" class="btn btn-block btn-success">Add</button>
                        </div>
                    </a>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable" product="grid" aria-describedby="example1_info">
                            <thead>
                            <tr product="row">
                                <th style="width: 493px;">Name</th>
                                <th tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 40px;"></th>
                                <th tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 40px;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($products as $product)
                                <?php $formName = "productRemove".$product->id ?>
                                <tr product="row" class="odd">
                                    <td class="sorting_1">{{$product->name}}</td>
                                    <td style="text-align: center;">
                                        <a href="{{action('Admin\ProductsController@edit',['product'=>$product->id])}}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td style="text-align: center;">
                                        <form name="<?=$formName;?>" method="POST" action="{{action('Admin\ProductsController@destroy',['product'=>$product->id])}}">
                                            <input type="hidden" name="_method" value="delete"/>
                                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                            <button type="submit" value="" class="fa fa-remove" style="background:none!important;border:none;color: #3c8dbc;" />
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(Session::has('message'))
                {{Session::get('message')}}
            @endif
        </div><!-- /.box-body -->
    </div>
@endsection