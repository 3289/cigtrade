@extends('admin.dashboard')
@section('content')

    <div class="col-md-6">

        @if(Session::has('message'))
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Massage</h4>
                {{Session::get('message')}}
            </div>
            @endif

                    <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit product</h3>
                </div><!-- /.box-header -->

                <form method="POST" action="{{action('Admin\ProductsController@update', ["product" => $product->id])}}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="Title">Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Title" value="{{$product->name}}">
                        </div>

                        <div class="form-group">
                            <label for="category_id">Категория</label>
                            <select name="type" id="type" class="form-control">
                                @foreach($categories as $category)
                                    <option @if($category->id==$product->category_id) selected @endif value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="type">Type</label>
                            <select name="type" id="type" class="form-control">
                                <option @if($product->type=='miner') selected @endif value="miner">Miner</option>
                                <option @if($product->type=='signal') selected @endif value="signal">Signal</option>
                                <option @if($product->type=='bot') selected @endif value="bot">Bots</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="price">Price</label>
                            <input type="text" name="price" class="form-control" placeholder="Price $" value="{{$product->price}}">
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea id="editor1" name="description">{{$product->description}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="order">Sort order</label>
                            <input type="text" name="sort_order" class="form-control" placeholder="order" value="{{$product->sort_order}}">
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="_method" value="put">
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>


            </div><!-- /.box-body -->
    </div><!-- /.box -->
@endsection

@push('js')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        CKEDITOR.replace('editor1');
    });
</script>
@endpush