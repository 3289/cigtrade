@extends('admin.dashboard')
@section('content')

    <div class="box">
        @if(Session::has('message'))
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i>Message</h4>
                {{Session::get('message')}}
            </div>
        @endif
        <div class="box-header">
            <h3 class="box-title">Categories List</h3>
            <div class="row" style="margin-top: 10px;">
                    <form method="GET" action="{{action('Admin\CategoriesController@create')}}">
                        <div class="col-sm-1 col-md-offset-0">
                            <button  class="btn btn-block btn-success">Add</button>
                        </div>
                    </form>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div id="tree" class="">
                <?php
                function renderCategories($tree, $parent_id = 0, $level = 0) {
                    if (isset($tree[$parent_id])) {

                        echo '<ul class="list-group level-' . $level . '">';
                        foreach ($tree[$parent_id] as $category) {
                            echo '<li class="list-group-item"><i class="fa fa-square"></i> ' . $category->name .
                                '<div class="options-container">'.
                                    '<div class="option-remove">'.
                                        '<form method="POST" action="./categories/'. $category->id .'">'.
                                            '<input type="hidden" name="_method" value="delete"/>'.
                                            '<input type="hidden" name="_token" value="'. csrf_token() .'"/>'.
                                            '<button type="submit" value="" class="fa fa-remove" style="background:none!important;border:none;color: #3c8dbc;" />
                                        </form>'.
                                    '</div>'.

                                    '<div class="option-edit">'.
                                        '<a href="categories/'. $category->id .'/edit">' .
                                        '<i class="fa fa-edit"></i></a>'.
                                    '</div>'.
                                 '</div>';

                            $level++;
                            renderCategories($tree, $category->id, $level);
                            $level--;

                            echo '</li>';
                        }
                        echo '</ul>';
                    }
                }

                renderCategories($tree);
                ?>
            </div>
        </div><!-- /.box-body -->
    </div>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            var subLists = $('[class*=level]:not(.level-0)');
            if (subLists.length)
                subLists.hide(function() {
                    $('.level-0').css({
                        opacity: 100
                    });
                });
            else
                $('.level-0').css({
                    opacity: 100
                });
            $('.list-group').click(function(e) {
                var selector = e.target.children[2] ? e.target.children[2] : '';
                if (selector)
                    $(selector).toggle('fast');

                e.stopPropagation();
            });
        });
    </script>

    <script>
        var btnArray = document.querySelectorAll('.fa-remove');
        for (var button in btnArray) {
            btnArray[button].addEventListener('click', function(e, opts) {
                var answer = confirm('Вы собираетесь удалить категорию, вы уверены ? Отменить удаление не возможно!');

                if (!answer) { e.preventDefault(); e.stopPropagation(); }
            });
        }
    </script>
@endsection