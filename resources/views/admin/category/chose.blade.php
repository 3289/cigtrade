<div style='display: inline; color: #73716e' id='chosenDiv'>
    <?php $renderList = []; ?>
    @foreach($previous as $prev)
        <?php array_push($renderList,
                    "<span class='address' style='display: inline-block; margin: 7px 5px 0 5px;'
                    onclick='render(".
                    ($prev->parent()?$prev->parent()->id:0).")'>". "$prev->name" ."</span>");
        ?>
    @endforeach
    <?php echo join("<i class='fa fa-fw fa-long-arrow-right'></i>", $renderList); ?>
    @if ($categories->count())
        @if ($previous)
            <i class='fa fa-fw fa-long-arrow-right'></i><br>
        @endif
        <select data-placeholder="Select category..." class="chosen col-md-12" onchange='render(this.options[this.selectedIndex].value)' title="Category">
            <option value="0">Top level category</option>
            @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
    @endif
    <input type='hidden' name='<?=$elementName;?>' id='<?=$elementName;?>' value='<?=$inputValue;?>' />
        @push('js')
        <script>
            $('.chosen').chosen({
                allow_single_deselect: true
            });
        </script>
        <script>
            function render(id) {
                $.ajax({
                    type:"GET",
                    url:'/ajax/categories/children/' + id + '/<?=$elementName;?>/{{ $except }}',
                    success: function(data){
                        $('#chosenDiv').html(data);
                    }
                });
            }
        </script>
        @endpush
</div>
