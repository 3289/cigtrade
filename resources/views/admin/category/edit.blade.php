@extends('admin.dashboard')
@section('content')
    <div class="col-md-6">
            @if(Session::has('message'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Message</h4>
                    {{Session::get('message')}}
                </div>
            @endif

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit category</h3>
                        <span class="lang_selector" id="lang_ru" style="padding: 5px; background-color: #00c0ef;"><img src="/share/flags/00_cctld/ru.png" /></span>
                        <span class="lang_selector" id="lang_en" style="padding: 5px"><img src="/share/flags/00_cctld/uk.png" /></span>
                    </div><!-- /.box-header -->

                <form method="POST" action="{{action('Admin\CategoriesController@update', ["category" => $category->id])}}" />
                    <div class="box-body">

                    <div class="form-group">
                        <label for="name">Category name</label>
                        <input id="name_ru" name="name_ru" type="text" placeholder="" class="form-control" value="{{ $category->name }}" required maxlength="255">
                        <input id="name_en" name="name_en" type="text" style="display: none" placeholder="" class="form-control" value="{{ $category->en()->name }}">
                    </div>

                    <div class="form-group">
                        <label for="order">Order</label>
                        <input id="order" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="order" type="text" placeholder="" class="form-control" value="{{ $category->order }}" maxlength="9" required>
                    </div>



                        <div class="form-group">
                            <label for="parent_id">Parent category</label>
                            <div class="category-wrapper">
                                <div id="chosenDiv">
                                    <input type="text" class="form-control" value="{{ $category->parent()->name }}" onclick="render()">
                                    <script>
                                        function render() {
                                            $.ajax({
                                                type:"GET",
                                                url:'/ajax/categories/children/0/parent_id/{{ $category->id }}',
                                                success: function(data){
                                                    $('#chosenDiv').html(data);
                                                }
                                            });
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="box-footer">
                    <button id="submit" name="addCategory" class="btn btn-primary">Save changes</button>

                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="_method" value="put">
                </div>
                </form>
                </div><!-- /.box-body -->
    </div><!-- /.box -->
@endsection