@extends('admin.dashboard')
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
                @if(Session::has('message'))
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-info"></i> Message</h4>
                        {{Session::get('message')}}
                    </div>
                @endif
                <div class="row">
                    <form method="POST" action="{{action('Admin\CategoriesController@store')}}" class="form-horizontal"/>
                    <fieldset>

                        <legend class="col-md-offset-1">Create new category</legend>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="name">Category name</label>
                            <div class="col-md-4">
                                <input id="name" name="name" type="text" placeholder="" maxlength="255" class="form-control input-md" required="">

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="order">Order</label>
                            <div class="col-md-4">
                                <input id="order" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="order" type="text" placeholder="" maxlength="9" class="form-control input-md" required="">

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="parent_id">Parent category</label>
                            <div class="col-md-4 category-wrapper">
                                <?php echo App::make('App\Http\Controllers\Admin\CategoriesController')->renderChose(0, 'parent_id'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="submit"></label>
                            <div class="col-md-4">
                                <button id="submit" name="addCategory" class="btn btn-primary">Add category</button>
                            </div>
                        </div>

                    </fieldset>
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    </form>
                </div>
        </div><!-- /.box-body -->
    </div>
@endsection