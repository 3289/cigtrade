@extends('admin.dashboard')
@section('content')


    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Invests list</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width: 40px;">ID</th>
                    <th>User</th>
                    <th style="width: 100px;">Amount</th>
                    <th style="width: 100px;">Package</th>
                    <th style="width: 100px;">Percents</th>
                    <th style="width: 40px;">Status</th>
                    <th style="width: 200px;">Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($invests as $invest)
                    <tr>
                        <td>{{$invest->id}}</td>
                        <td>{{$invest->user->name}}</td>
                        <td>{{$invest->package->name}}</td>
                        <td>{{$invest->package_term->percents}} %</td>
                        <td>{{$invest->amount}}</td>
                        <td>{{$invest->state}}</td>
                        <td>{{($invest->created_at)?$invest->created_at->format('d.m.Y H:i:s'):""}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @if(Session::has('message'))
            {{Session::get('message')}}
        @endif
    </div><!-- /.box-body -->
    </div>
@endsection