{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Dashboard</h1>
@stop

@section('content')
@stop



@section('css')
    <link rel="stylesheet" href="{{asset('/css/chosen.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/categories.css')}}">
    <link rel="stylesheet" href="{{asset('/css/autocomplete.css')}}">
@stop

@section('js')
    <script src="{{asset('/js/chosen.jquery.js')}}"></script>
    <script src="{{asset('/js/lang.js')}}"></script>
@stop