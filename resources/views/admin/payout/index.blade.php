@extends('admin.dashboard')
@section('content')


    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Payouts list</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width: 40px;">ID</th>
                    <th>User</th>
                    <th style="width: 100px;">Amount</th>
                    <th style="width: 40px;">Status</th>
                    <th style="width: 200px;">Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($payouts as $payout)
                    <tr class='clickable-row' data-href='{{action('Admin\PayoutsController@item', ['id' => $payout->id])}}'>
                        <td>{{$payout->id}}</td>
                        <td>{{$payout->user->name}}</td>
                        <td>{{$payout->amount}}</td>
                        <td>{{$payout->state()}}</td>
                        <td>{{($payout->created_at)?$payout->created_at->format('d.m.Y H:i:s'):""}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @if(Session::has('message'))
            {{Session::get('message')}}
        @endif
    </div><!-- /.box-body -->
    </div>
@endsection

@push('js')
<script type="application/javascript">
    $(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
</script>
@endpush