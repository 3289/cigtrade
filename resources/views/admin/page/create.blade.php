@extends('admin.dashboard')
@section('content')

    <div class="col-md-12">

        @if(Session::has('message'))
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Massage</h4>
                {{Session::get('message')}}
            </div>
            @endif

                    <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add new page</h3>
                </div><!-- /.box-header -->

                <form method="POST" action="{{action('Admin\PagesController@store')}}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="Title">Title</label>
                            <input type="text" maxlength="255" name="title" class="form-control" placeholder="Title">
                        </div>

                        <div class="form-group">
                            <label for="locality">Alias</label>
                            <input type="text" name="alias" maxlength="255" class="form-control" placeholder="Alias">
                        </div>

                        <div class="form-group">
                            <textarea id="editor1" name="body" rows="10" cols="80"></textarea>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>


            </div><!-- /.box-body -->
    </div><!-- /.box -->

@endsection

@push('js')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        CKEDITOR.replace('editor1');
    });
</script>
@endpush