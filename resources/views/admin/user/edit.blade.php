@extends('admin.dashboard')

@push('js')
    <script src="{{asset('js/autocomplete.js')}}"></script>
@endpush

@push('css')
    <link rel="stylesheet" href="{{asset('css/autocomplete.css')}}">
@endpush

@section('content')

    <div class="col-md-6">

        @if(Session::has('message'))
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Massage</h4>
                {{Session::get('message')}}
            </div>
            @endif

                    <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit user account</h3>
                </div><!-- /.box-header -->

                <form method="POST" action="{{action('Admin\UsersController@update', ["user" => $current->id])}}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Name" value="{{$current->name}}">
                        </div>

                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="text" name="email" class="form-control" placeholder="E-Mail" value="{{$current->email}}">
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" placeholder="">
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="_method" value="put">
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>

            </div><!-- /.box-body -->
    </div><!-- /.box -->

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Edit user roles</h3>
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    <label for="role">Add role to user</label>
                    <input id="role" type="hidden" placeholder="Role name" class="form-control" />
                    <input type="button" id="addRole" class="btn btn-primary" value="Add Role" style="margin-top:10px"/>
                </div>

                <table  class="table table-bordered table-striped dataTable" role="grid">
                    <thead>
                    <tr>
                        <th tabindex="0" rowspan="1" colspan="1"  style="width: 493px;">Role name</th>
                        {{--<th tabindex="0" rowspan="1" colspan="1" style="width: 40px;"></th>--}}
                        <th tabindex="0" rowspan="1" colspan="1" style="width: 40px;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($current->roles as $role)
                        <tr role="row" class="odd">
                            <td>{{$role->name}}</td>
                            <td style="text-align: center;">
                                    <button class="fa fa-remove deprive" data-id="{{ $role->id }}" style="background:none!important;border:none;color:#3c8dbc;"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var userId = '{{ $current->id }}';

            var rolesOptions = {
                token: '{{ csrf_token() }}',
                url: '/ajax/roles/getPredictions',
                selector: '#role',
                valueField: 'id',
                displayField: 'name'
            };

            var rolesFilter = new Filter(rolesOptions);

            $('.deprive').click(function() {
                var roleId = this.dataset.id;
                var me = this;

                $.ajax({
                    method: 'POST',
                    url: '/ajax/roles/deprive',
                    data: {
                        userId: userId,
                        roleId: roleId,
                        _token: '{{ csrf_token() }}'
                    },
                    success: function (resp) {
                        $(me).parents('tr[role=row]').remove();
                    },
                    error: function (resp) {
                        $(document.body).html(resp.responseText);
                    }
                });
            });

            $('#addRole').click(function() {
                var roleId = $('#role').val();
                if (!roleId) {
                    alert('Select role first!');
                    return;
                }
                $.ajax({
                    method: 'POST',
                    url: '/ajax/roles/give',
                    data: {
                        userId: userId,
                        roleId: roleId,
                        _token: '{{ csrf_token() }}'
                    },
                    success: function (resp) {
                        $('.role' + rolesFilter.instanceKey).val('');
                        document.location += '';
                    },
                    error: function (resp) {
                        $(document.body).html(resp.responseText);
                    }
                });
            });
        });
    </script>
@endsection
