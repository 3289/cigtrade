@extends('admin.dashboard')
@section('content')


     <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Users list</h3>
            <div class="box-tools">
                <div class="no-margin pull-right">
                    <a href="{{action('Admin\UsersController@create')}}">
                            <button type="submit" class="btn btn-block btn-success">Add</button>
                    </a>
                </div>
            </div>
        </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 493px;">Name</th>
                                <th style="width: 493px;">email</th>
                                <th style="width: 493px;">registered</th>
                                <th style="width: 40px;"></th>
                                <th style="width: 40px;"></th>
                                <th style="width: 40px;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <?php $formName = "userRemove".$user->id ?>
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{($user->created_at)?$user->created_at->format('d.m.Y H:i:s'):""}}</td>
                                    <td style="text-align: center;">
                                        <a href="{{action('Admin\UsersController@edit',['user'=>$user->id])}}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>

                                    @if($user->blocked)
                                        <td style="text-align: center;">
                                            <form method="POST" action="{{action('Admin\UsersController@unBlock',['user'=>$user->id])}}">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                <button type="submit" value="" class="fa fa-unlock" style="background:none!important;border:none;color: #3c8dbc;" />
                                            </form>
                                        </td>
                                    @else
                                        <td style="text-align: center;">
                                            <form method="POST" action="{{action('Admin\UsersController@block',['user'=>$user->id])}}">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                <button type="submit" value="" class="fa fa-lock" style="background:none!important;border:none;color: #3c8dbc;" />
                                            </form>
                                        </td>
                                    @endif

                                    <td style="text-align: center;">
                                        <form name="<?=$formName;?>" method="POST" action="{{action('Admin\UsersController@destroy',['user'=>$user->id])}}">
                                            <input type="hidden" name="_method" value="delete"/>
                                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                            <button type="submit" value="" class="fa fa-remove" style="background:none!important;border:none;color: #3c8dbc;" />
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
            @if(Session::has('message'))
                {{Session::get('message')}}
            @endif
        </div><!-- /.box-body -->
    </div>
@endsection