<?php

namespace App\Http\Controllers;

use App\Invest;
use App\Payout;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PayoutController extends Controller
{
    public static function allowPayout($id)
    {
        $invest = Invest::find($id);
        $payout = Payout::where('user_id', '=', Auth::user()->id)
            ->where('package_id', '=', $invest->package_id)
            ->where('term_id', '=', $invest->term_id)->where('done', 0)->count();
        return !$payout;
    }

    private function countAmount(Request $request)
    {
        $amount = 0;
        $type = $request->type;
        if ($type == 'referal') {
            $invest = Invest::find($request->id);
            if ($invest->user->referal_id == Auth::user()->id) {
                $amount = $invest->amount * 0.1;
            } else {
                return back();
            }
        }
        return $amount;
    }

    public function makeReferal(Request $request)
    {
        $user = \Auth::user();
        if (!$user) {
            abort(404);
        }
        $referals = $user->getReferals();
        foreach ($referals as $referal) {
            Payout::create([
                'type' => 'referal',
                'amount' => $referal->amount,
                'done' => false,
                'requisites' => $request->requisites,
                'package_id' => $referal->package_id,
                'term_id' => $referal->term_id,
                'invest_id' => $referal->id
            ]);
        }

        $message = 'Реквизиты: ' . $request->requisites . "\n" .
            'Сумма:' . array_sum($referals->pluck('amount')->toArray()) * 0.1 . ' $';
        \Mail::raw($message, function (\Illuminate\Mail\Message $mail) use ($request) {
            $mail->subject('Выплата реферальных');
            $mail->from('pisarjevsky@gmail.com', 'cigtrade.com');
            $mail->to('demhenko@dlit.dp.ua');
        });
        return redirect(action("PayoutController@index"));
    }

    public function orderReferal()
    {
        $user = \Auth::user();
        if (!$user) {
            abort(404);
        }
        $referals = $user->getReferals();

        return view('payout.orderReferal', [
            'referals' => $referals
        ]);
    }

    public
    function order(Request $request)
    {
        if (!$this->allowPayout($request->id)) {
            return back();
        }
        $amount = $this->countAmount($request);
        return view('payout.order', ['amount' => $amount, 'id' => $request->id, 'type' => $request->type]);
    }

    public
    function order_invest(Request $request)
    {
        $amount = $request->amount;
        return view('payout.order', ['amount' => $amount]);
    }

    public
    function make_invest(Request $request)
    {
        $invest = Invest::find($request->id);
        $amount = $invest->getPayoutAmount();
        Payout::create([
            'type' => 'invest',
            'amount' => $amount,
            'done' => false,
            'requisites' => $request->requisites,
            'package_id' => $invest->package_id,
            'term_id' => $invest->term_id
        ]);
        return redirect(action("PayoutController@index"));
    }

    public
    function index()
    {
        return view('profile.payouts');
    }
}
