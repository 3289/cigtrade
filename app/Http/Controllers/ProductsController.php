<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderHistory;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
    public function buy(Request $request)
    {
        $product = Product::find($request->id);
        $user = Auth::user();
        $order = Order::create([
            'user_id' => $user->id,
            'product_id' => $product->id,
            'comment' => $request->comment,
            'state' => 'payed',
            'amount' => $product->price
        ]);

        OrderHistory::create([
            'order_id' => $order->id,
            'state' => $order->state
        ]);

        return redirect(action('ProfileController@orders'));
    }

    public function index()
    {
        return view('product.index', ['products' => Product::where('type', '=', 'miner')->get()]);
    }

    public function signals()
    {
        return view('product.signals', ['products' => Product::where('type', '=', 'signal')->get()]);
    }

    public function item($id)
    {
        $product = Product::find($id);
        return view('product.item', ['product' => $product]);
    }
}
