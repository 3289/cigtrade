<?php

namespace App\Http\Controllers;

use App\Country;
use App\Invest;
use App\Package;
use App\PackageTerm;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function home()
    {
        return view('profile.home');
    }

    public function support()
    {
        return view('profile.support');
    }

    public function send(Request $request)
    {
        $request->validate([
            'theme' => 'required',
            'question' => 'required|min:10',
        ]);

        $user = \Auth::user();
        $topic = $request->theme == 'payout' ? 'Запрос на вывод' : ($request->theme == 'consult' ? 'Запрос на консультацию' : 'Технические вопросы');

        $message = 'Пользователь: ' . "\n" . 'id: ' . $user->id . "\n" . 'Имя: ' . $user->name . "\n" . "\n" . 'Вопрос: ' . $request->question;
        \Mail::raw($message, function (\Illuminate\Mail\Message $mail) use ($topic) {
            $mail->subject($topic);
            $mail->from('pisarjevsky@gmail.com', 'cigtrade.com');
            $mail->to('demhenko@dlit.dp.ua');
        });
        return view('profile.home');
    }

    public function index()
    {
        $countries = Country::all()->pluck('name', 'id'); // get all countries
        return view('profile.profile', ['countries' => $countries]);
    }

    public function invest($id)
    {
        $package = Package::find($id);
        return view('profile.invest', ['package' => $package]);
    }

    public function make_invest(Request $request)
    {
        $data = $request->all();
        $term = PackageTerm::find($request->termId);
        $invest = Invest::create([
            'user_id' => Auth::getUser()->id,
            'amount' => $request->amount,
            'term' => $term->term,
            'percents' => $term->percents,
            'package_id' => $term->package->id,
            'term_id' => $term->id
        ]);
        return response()->json([
            'id' => $invest->id,
            'amount' => $request->amount
        ]);
    }

    public function invests()
    {
        $packages = Package::all();
        return view('profile.invests', ['packages' => $packages]);
    }

    public function referals()
    {
        return view('profile.referals');
    }

    public function orders()
    {
        return view('profile.orders');
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update([
            'name' => $request->name,
            'surname' => $request->surname,
            'middle_name' => $request->middle_name,
            'phone' => $request->phone,
            'country_id' => $request->country_id,
            'viber' => $request->viber,
            'skype' => $request->skype,
            'telegram' => $request->telegram
        ]);
        if (!empty($request["password"]) && $request["password"] == $request["password_confirmation"]) {
            $user->update(['password' => bcrypt($request["password"])]);
        }
        return back()->with('message', "User account " . $user->name . " edited");
    }
}
