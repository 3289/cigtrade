<?php

namespace App\Http\Controllers;

use App\Package;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::all();
        return view('home', ['packages' => $packages]);
    }

    public function register($id)
    {
        $id = User::getRealId($id);

        $user = User::find($id);

        if ($user !== null) {

            session(['referal' => $user->id]);
        }
        return redirect(action('Auth\RegisterController@register'));
    }
}
