<?php

namespace App\Http\Controllers;

use App\Invest;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function nixmoney(Request $request)
    {
        $invest = Invest::find($request->PAYMENT_ID);
        $invest->payed();
        return redirect(action('ProfileController@invests'));
    }

    public function nixmoney_fail(Request $request)
    {
        $invest = Invest::find($request->PAYMENT_ID);
        $invest->delete();
        return redirect(action('ProfileController@index'));
    }

    public function cryptonator(Request $request)
    {
        $invest = Invest::find($request->PAYMENT_ID);
        $invest->payed();
        return redirect(action('ProfileController@invests'));
    }

    public function cryptonator_fail(Request $request)
    {
        $invest = Invest::find($request->PAYMENT_ID);
        $invest->delete();
        return redirect(action('ProfileController@index'));
    }
}
