<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Page;
use App\Http\Requests;

class PagesController extends Controller
{
    public function index()
    {
        $pages = Page::all();
        return view('admin.page.index',['pages'=>$pages]);
    }

    public function create()
    {
        $parents = Page::all();
        return view('admin.page.create', ['parents' => $parents]);
    }

    public function store(Request $request)
    {
        Page::create($request->all());
        return back()->with('message','Page created');
    }

    public function edit($id)
    {
        $page=Page::find($id);
        return view('admin.page.edit',['page'=>$page]);
    }

    public function update(Request $request, $id)
    {
        $page = Page::find($id);
        $page->update($request->all());
        return back()->with('message', "Page ".$page->title." edited");
    }

    public function destroy($id)
    {
        $page=Page::find($id);
        $page->delete();
        return back()->with('message', "Page ".$page->title." deleted");
    }
}
