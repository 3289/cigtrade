<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Role;
use App\User;
use DB;

class RolesController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return view('admin.role.index',['roles'=>$roles]);
    }

    public function create()
    {
        return view('admin.role.create');
    }

    public function store(Request $request)
    {
        Role::create($request->all());
        return back()->with('message','Role created');
    }

    public function edit($id)
    {
        $role=Role::find($id);
        return view('admin.role.edit',['role'=>$role]);
    }

    public function update(Request $request, $id)
    {
        $role = Role::find($id);
        $role->update($request->all());
        return back()->with('message', "Role ".$role->name." edited");
    }

    public function destroy($id)
    {
        $role=Role::find($id);
        $role->delete();
        return back()->with('message', "Role ".$role->name." deleted");
    }

    public function getPredictions(Request $request) {
        $filter = $request->filter;
        $count = $request->limit;

        $roles = Role::where('name', 'ilike', '%'.$filter.'%')
            ->limit($count)
            ->get();

        $response = [];
        $response['data'] = $roles->toArray();
        $response['status'] = 'OK';

        return $response;
    }
    
    public function give(Request $request) {
        $user = User::find($request->userId);
        $role = Role::find($request->roleId);

        if (!in_array($role->name, $user->rolesList())) {
            DB::table('role_user')->insert([
                'role_id' => $role->id,
                'user_id' => $user->id
            ]);

            return ['success' => true];
        } else {
            return ['success' => false, 'message' => 'User already have this role'];
        }
    }
    
    public function deprive(Request $request) {
        $user = User::find($request->userId);
        $role = Role::find($request->roleId);

        if ($user->hasRole([$role->name])) {
            DB::table('role_user')
                ->where('user_id', '=', $user->id)
                ->where('role_id', '=', $role->id)
                ->delete();

            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }
}
