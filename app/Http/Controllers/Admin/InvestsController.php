<?php

namespace App\Http\Controllers\Admin;

use App\Invest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvestsController extends Controller
{
    public function index()
    {
        $invests = Invest::all();
        return view('admin.invest.index', ['invests'=> $invests]);
    }
}
