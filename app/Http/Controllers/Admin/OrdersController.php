<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\OrderHistory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    public function index()
    {
        $orders = Order::all();
        return view('admin.order.index',['orders'=>$orders]);
    }

    public function item($id)
    {
        $order = Order::find($id);
        return view('admin.order.order', ['order' => $order]);
    }

    public function confirm(Request $request)
    {
        $order = Order::find($request->id);
        $order->update([
            'state' => 'done'
        ]);
        OrderHistory::create([
            'order_id' => $order->id,
            'state' => $order->state
        ]);
        return redirect(action('Admin\OrdersController@index'));
    }
}
