<?php

namespace App\Http\Controllers\Admin;

use App\Package;
use App\PackageTerm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackagesController extends Controller
{
    public function index()
    {
        $packages = Package::all();
        return view('admin.package.index', ['packages'=>$packages]);
    }

    public function create()
    {
        return view('admin.package.create');
    }

    public function store(Request $request)
    {
        Package::create($request->all());
        return back()->with('message','Package created');
    }

    public function edit($id)
    {
        $package = Package::find($id);
        return view('admin.package.edit',['package'=>$package]);
    }

    public function update(Request $request, $id)
    {
        $package = Package::find($id);
        $package->update($request->all());
        return back()->with('message', "Package ".$package->name." edited");
    }

    public function destroy($id)
    {
        $package = Package::find($id);
        $package->delete();
        return back()->with('message', "Package ".$package->name." deleted");
    }

    public function add_term(Request $request)
    {
        $term = PackageTerm::create($request->all());
        return ['success' => true, 'id' => $term->id];
    }

    public function remove_term(Request $request)
    {
        $term = PackageTerm::find($request->termId);
        $term->delete();
        return ['success' => true];
    }
}
