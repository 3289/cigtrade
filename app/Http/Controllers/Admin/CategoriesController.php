<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Category;

class CategoriesController extends Controller
{

    public function index()
    {
        $categories = Category::OrderAlphabetical()->get();

        $tree = array();
        foreach($categories as $category) {
            $tree[$category->parent_id][] = $category;
        }

        return view('admin.category.index',['tree'=>$tree]);
    }

    public function create()
    {
        $categories = Category::where('parent_id', '=', 0)->get();
        return view('admin.category.create',['categories' => $categories]);
    }

    public function store(Request $request)
    {
        $category = Category::create($request->all());
        return back()->with('message','Category created');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.category.edit',
            [
                'category'=>$category
            ]);
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $oldName = $category->name;
        $category->update($request->all());

        if ($oldName != $request->name)
            return back()->with('message', "Category ". $oldName ." renamed to ". $category->name);
        else
            return back()->with('message', "Category updated!");
    }

    public function destroy($id)
    {
        $category=Category::find($id);
        $category->delete();
        return back()->with('message', "Category ".$category->name." has been deleted");
    }

    public function renderChose($id, $elementName, $except = 0)
    {
        $stop = Category::find($id);
        $categories = Category::where('id', '<>', $except)
            ->where('parent_id', '=', $id)
            ->get();
        $previous = [];
        if ($id) {
            do {
                $parent = Category::find($id);
                array_unshift($previous, $parent);
                $id = $parent->parent_id;
            } while ($id);
        }
        return view('admin.category.chose',['categories' => $categories,
            'previous' => $previous,
            'inputValue' => (($stop)?$stop->id:0),
            'elementName' => $elementName,
            'except' => $except
        ]);
    }
    
    public function getChildren(Request $request) {
        $parent_id = $request->parent_id;
        $categories = Category::where('parent_id', '=', $parent_id)
            ->orderBy('order', 'asc')
            ->get()->toArray();

        $result = [
            'data' => $categories
        ];

        if (!count($categories))
            $result['status'] = 'END_CATEGORY';
        else
            $result['status'] = 'OK';

        return $result;
    }

    public function getParentsChain(Request $request)
    {
        $chainTo = request('chain_to');
        $currentItem = Category::find($chainTo)->toArray();
        $categoriesChain = [$currentItem];

        do {
            $currentItem = Category::find($currentItem['parent_id'])->toArray();
            array_unshift($categoriesChain, $currentItem);
        } while ($currentItem['id']);

        array_shift($categoriesChain);
        return [
            'status' => 'OK',
            'chain' => $categoriesChain
        ];
    }

    public function checkEndCategory ($id) {
        $category = Category::where('parent_id', '=', $id)->limit(1)->get();

        if (count($category)) {
            return ['success' => false];
        }
        return ['success' => true];
    }
}
