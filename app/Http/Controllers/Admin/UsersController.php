<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\Organization;
use Auth;
use DB;
use Hash;
use Image;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all()->sortByDesc('created_at');

        return view('admin.user.index',['users' => $users]);
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(Request $request)
    {
        User::create([ 'name' => $request["name"],
            'email' => $request["email"],
            'password' => bcrypt($request["password"])]);
        return back()->with('message','User account created');
    }

    public function edit($id)
    {
        $user = User::find($id);
        
        return view('admin.user.edit',['current' => $user]);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update([ 'name' => $request["name"],
                        'email' => $request["email"]]);
        if (!empty($request["password"])) {
            $user->update(['password' => bcrypt($request["password"])]);
        }
        return back()->with('message', "User account ".$user->name." edited");
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return back()->with('message', "User account ".$user->name." deleted");
    }

    public function block($id) {
        $user = User::find($id);
        $user->update([ 'blocked' => true]);
        return back()->with('message', "User account ".$user->name." blocked");
    }

    public function unBlock($id) {
        $user = User::find($id);
        $user->update([ 'blocked' => false]);
        return back()->with('message', "User account ".$user->name." unblocked");
    }
    
    public function editPass() {
        return view('auth.passwords.change');
    }

    public function checkOldPass(Request $request) {
        $authPass = \Auth::user()->getAuthPassword();
        $currentPass = $request->oldPass;

        if (Hash::check($currentPass, $authPass)) {
            return ['status' => 'OK'];
        }

        return ['status' => 'FAIL'];
    }

    public function changePass(Request $request) {
        $user = User::find(Auth::user()->id);

        $newPass = Hash::make($request->password);

        if (!Hash::check($request->old_password, \Auth::user()->getAuthPassword())) {
            \Session::set('old_pass', 'error');
            return back();
        } else if ($request->password != $request->password_confirmation) {
            return back()->withErrors(['password_confirmation' => 'fail']);
        }

        $user->update(['password' => $newPass]);

        return view('auth.passwords.changed');
    }

    public function loadPhoto(Request $request) {
        $userId = Auth::getUser()->id;

        $photo = $request->file;

        $folderPath = "uploads/users/$userId";

        if (!file_exists($folderPath)) {
            mkdir($folderPath, 0777, true);
        }

        $image = Image::make($photo);
        $image_small = Image::make($photo);
        $image_big = Image::make($photo);

        $image_small->fit(80, 80)->save("$folderPath/photo_small.png");
        $image_big->fit(300, 300)->save("$folderPath/photo_big.png");
        $image->save("$folderPath/photo_original.png");

        return ['status' => 'OK'];
    }

    public function getPredictions(Request $request) {
        $filter = $request->filter;
        $count = $request->limit;

        $users = User::where('email', 'ilike', '%'.$filter.'%')
            ->limit($count)
            ->get();

        $response = [];
        $response['data'] = $users->toArray();
        $response['status'] = 'OK';

        return $response;
    }

    public function newUsersCount() {
        $week = User::where('created_at', '>', date('Y-m-d H:i:s',strtotime("-1 week")))->count();
        $today = User::where('created_at', '>', date('Y-m-d H:i:s',strtotime("-1 day")))->count();

        return [
            'week' => $week != 0 ? "+$week" : $week,
            'today' => $today != 0 ? "+$today" : $today
        ];
    }
    
    public function online(Request $request)
    {
        $user = User::find($request["user"]);
        $user->update([
            'state' => 'online'
        ]);
    }

    public function offline(Request $request)
    {
        $user = User::find($request["user"]);
        $user->update([
            'state' => 'offline'
        ]);
    }
}
