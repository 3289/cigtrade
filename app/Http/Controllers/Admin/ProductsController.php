<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('admin.product.index',['products'=>$products]);
    }

    public function create()
    {
        $categories = Category::where('id', '>', 0)->get();
        return view('admin.product.create', ['categories' => $categories]);
    }

    public function store(Request $request)
    {
        Product::create($request->all());
        return back()->with('message','product created');
    }

    public function edit($id)
    {
        $product=Product::find($id);
        $categories = Category::where('id', '>', 0)->get();
        return view('admin.product.edit',['product'=>$product, 'categories' => $categories]);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->update($request->all());
        return back()->with('message', "product ".$product->title." edited");
    }

    public function destroy($id)
    {
        $product=Product::find($id);
        $product->delete();
        return back()->with('message', "product ".$product->title." deleted");
    }
}
