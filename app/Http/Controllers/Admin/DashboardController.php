<?php

namespace App\Http\Controllers\Admin;

use App\Invest;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function dashboard()
    {
        //$week = User::where('created_at', '>', date('Y-m-d H:i:s',strtotime("-1 week")))->count();
        $today = User::where('created_at', '>', date('Y-m-d H:i:s',strtotime("-1 week")))->count();
        $orders = Order::where('created_at', '>', date('Y-m-d H:i:s',strtotime("-1 week")))->count();
        $money = Invest::where('created_at', '>', date('Y-m-d H:i:s',strtotime("-1 week")))->sum('amount');

        /*return [
            'week' => $week != 0 ? "+$week" : $week,
            'today' => $today != 0 ? "+$today" : $today
        ];*/
        return view('admin.index', [
            'users' => $today != 0 ? "+$today" : $today,
            'orders' => $orders,
            'money' => $money]);
    }
}
