<?php

namespace App\Http\Controllers\Admin;

use App\Invest;
use App\Package;
use App\PackageTerm;
use App\User;
use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{
    public function users($type = 'bar', $month = 0, $year = 0)
    {
        $month = $month ? $month : date('m');
        $year = $year ? $year : date('Y');
        $title = "Зарегистрированые пользователи";
        $users = User::all();//where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"), date('Y'))->get();
        $chart = Charts::database($users, $type, 'highcharts')
            ->title($title)
            ->elementLabel("Пользователи")
            ->dimensions(0, 500)
            ->responsive(false)
            ->groupByDay($month, $year, false);
        $action = action('Admin\StatisticsController@users');
        return view('admin.charts.users',compact('chart', 'action', 'title', 'type', 'month', 'year'));
    }

    public function userTerms($type = 'bar', $month = 0, $year = 0)
    {
        $month = $month ? $month : date('m');
        $year = $year ? $year : date('Y');
        $title = "Пользователи по программам";
        $terms = PackageTerm::all();
        $chart = Charts::multiDatabase($type, 'highcharts')
            ->title($title)
            ->elementLabel("Пользователи")
            ->dimensions(0, 500);

        for($i=0; $i < $terms->count(); $i++) {
            $chart->dataset($terms[$i]->package->name." ".$terms[$i]->percents." %", Invest::where('term_id', '=', $terms[$i]->id)->get());
        }
        $chart->groupByDay($month, $year, false);

        $action = action('Admin\StatisticsController@userTerms');
        return view('admin.charts.userTerms',compact('chart', 'action', 'title', 'type', 'month', 'year'));
    }

    public function userPackages($type = 'bar', $month = 0, $year = 0)
    {
        $month = $month ? $month : date('m');
        $year = $year ? $year : date('Y');
        $title = "Пользователи по пакетам";
        $packages = Package::all();
        $chart = Charts::multiDatabase($type, 'highcharts')
            ->title($title)
            ->elementLabel("Пользователи")
            ->dimensions(0, 500);

        for($i=0; $i < $packages->count(); $i++) {
            $chart->dataset($packages[$i]->name, Invest::where('package_id', '=', $packages[$i]->id)->get());
        }

        $chart->groupByDay($month, $year, false);

        $action = action('Admin\StatisticsController@userPackages');
        return view('admin.charts.userPackages',compact('chart', 'action', 'title', 'type', 'month', 'year'));
    }
}
