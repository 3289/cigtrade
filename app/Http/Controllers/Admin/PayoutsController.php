<?php

namespace App\Http\Controllers\Admin;

use App\Payout;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PayoutsController extends Controller
{
    public function index()
    {
        $payouts = Payout::all();
        return view('admin.payout.index', ['payouts'=> $payouts]);
    }

    public function item($id)
    {
        $order = Payout::find($id);
        return view('admin.payout.order', ['order' => $order]);
    }

    public function confirm(Request $request)
    {
        $order = Payout::find($request->id);
        $order->update([
            'done' => true
        ]);
        return redirect(action('Admin\PayoutsController@index'));
    }
}
