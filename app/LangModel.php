<?php

namespace App;

use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class LangModel extends Model
{
    protected $locale;
    protected $table;
    protected $translations;
    protected static $data = [];

    public function __get($name)
    {
        if (!isset($this->locale))
            $locale = \App::getLocale();
        else
            $locale = $this->locale;
        if (in_array($name, $this->translations) && $locale !== Config::get("lang.default")) {
            $itemID = parent::__get('id');
            $fieldName = $this->table . "_" . $itemID . "_" . $name . "_" . $locale;
            if (!isset(static::$data[$fieldName])) {
                $table = $this->table;
                $this->table = $this->table . "_" . $locale;
                $item = parent::__call('find', [$itemID]);
                $this->table = $table;
                if ($item !== null) {
                    $item = $item->toArray();
                    static::$data[$fieldName] = $item[$name];
                } else {
                    static::$data[$fieldName] = parent::__get($name);
                }
            }
            return static::$data[$fieldName];
        } else {
            return parent::__get($name);
        }
    }

    public function __call($method, $parameters) {
        if (strlen($method) == 2 && in_array($method, Config::get("lang.localities"))) {
            $this->locale = $method;
            return $this;
        }
        return parent::__call($method, $parameters);
    }

    public function delete() {
        $itemID = parent::__get('id');
        foreach(Config::get("lang.localities") as $locale) {
            if ($locale !== Config::get("lang.default")) {                
                \DB::delete('delete from ' . $this->table . "_" . $locale . " where id = ?", array($itemID));
            }
        }
        return parent::delete();
    }

    public function update(array $attributes = [], array $options = []) {
        $itemID = parent::__get('id');
        foreach($this->translations as $item) {
            $attributes[$item] = $attributes[$item."_".Config::get("lang.default")];
        }

        foreach(Config::get("lang.localities") as $locale) {
            if ($locale !== Config::get("lang.default")) {
                $values = [$itemID];
                $questions = "?";
                foreach($this->translations as $field) {
                    array_push($values, $attributes[$field . "_" . $locale]);
                    $questions .= ",?";
                }
                \DB::delete('delete from ' . $this->table . "_" . $locale . " where id = ?", array($itemID));
                \DB::insert('insert into ' . $this->table . '_' . $locale . ' (id, '.implode(",", $this->translations).') values ('.$questions.')', $values);
            }
        }
        return parent::update($attributes, $options);
    }

    public function getTranslations() {
        return $this->translations;
    }
}