<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Invest extends Model
{
    protected $table = "invests";
    protected $fillable = ['user_id', 'term', 'percents', 'state', 'start', 'amount', 'package_id', 'term_id'];
    protected $dates = ['start'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function package()
    {
        return $this->belongsTo('App\Package');
    }

    public function package_term()
    {
        return $this->belongsTo('App\PackageTerm', 'term_id');
    }

    public function payed()
    {
        $this->start = new \DateTime();
        $this->state = 'payed';
        $this->update();
    }

    public function earned()
    {
        $days = (int)$this->past();
        return $this->amount * ($this->percents/($this->term * 100)) * $days;
    }

    public function getPayoutAmount()
    {
        $amount = $this->amount;
        $bonus = $this->amount * ($this->percents - 100)/100;
        return $amount + $bonus;
    }

    public function past()
    {
        $fdate = $this->start;
        $datetime1 = new DateTime($fdate);
        $datetime2 = new DateTime();
        $interval = $datetime1->diff($datetime2);
        $days = (int)$interval->format('%a');
        return $days;
    }

    public function left()
    {
        return $this->term - $this->past();
    }

    public function monthly()
    {
        return $this->package->monthly;
    }
}
