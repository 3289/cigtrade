<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class OrderHistory extends Model
{
    protected $table = "history";
    protected $fillable = ["order_id", 'state'];

    public static function boot()
    {
        parent::boot();
        OrderHistory::creating(function($item){
            $item->user_id = Auth::user()->id;
        });
    }

}
