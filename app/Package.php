<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = "package";
    protected $fillable =
        [
            'name',
            'amount_min',
            'amount_max',
            'description',
            'sort_order',
            'monthly'
        ];

    public function terms()
    {
        return $this->hasMany('App\PackageTerm', 'package_id');
    }

    public function termsList()
    {
        return $this->terms()->pluck("name")->all();
    }
}
