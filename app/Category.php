<?php

namespace App;

class Category extends LangModel
{
    protected $table="categories";
    protected $fillable=['name', 'parent_id', 'order'];
    protected $translations = array('name');

    public function parent()
    {
        return Category::find($this->parent_id);
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function scopeOrderAscending($query)
    {
        return $query->orderBy('order','asc');
    }

    public function scopeOrderAlphabetical($query)
    {
        return $query->orderBy('name','asc');
    }
}
