<?php

namespace App;

use App\Http\Controllers\PayoutController;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'surname', 'middle_name', 'phone', 'viber', "skype", "telegram", "country_id", 'referal_id', 'blocked'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_user', 'user_id', 'role_id');
    }

    public function invests()
    {
        return $this->hasMany('App\Invest');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function payouts()
    {
        return $this->hasMany('App\Payout');
    }

    public function referals()
    {
        return $this->hasMany('App\User', 'referal_id');
    }

    public function rolesList()
    {
        return $this->roles()->pluck("name")->all();
    }

    public function hasRole($roles)
    {
        $userRoles = \Auth::user()->rolesList();
        foreach ($roles as $role) {
            if (in_array($role, $userRoles)) {
                return true;
            }
        }
        return false;
    }

    public static function getRealId($id)
    {
        return substr($id, 3, mb_strlen($id) - 3);
    }

    public static function getReferalId($id)
    {
        return substr(mb_strrchr($id, '379'), 3, mb_strlen(mb_strrchr($id, '379')) - 3);
    }

    public function getTotalAmount()
    {
        $invests = $this->referals()->select('invests.id')
            ->join('invests', 'users.id', '=', 'invests.user_id')
            ->select('invests.id', 'amount')->get()->toArray();
        $amount = [];
        foreach ($invests as $invest) {
            $amount[] = $invest['amount'];
        }
        return $amount;
    }

    public function getReferals()
    {
        $payouts = Payout::all()->pluck('invest_id', 'id')->toArray();

        $referalsInfo = $this->referals()
            ->select('invests.user_id', 'invests.amount', 'invests.package_id', 'invests.term_id', 'invests.start', 'invests.id', 'users.name')
            ->join('invests', 'users.id', '=', 'invests.user_id')
            ->whereNotIn('invests.id', $payouts)->get();
        return $referalsInfo;
    }

    public function getReferalInvestsIds()
    {
        $invests = $this->referals()->select('invests.id')
            ->join('invests', 'users.id', '=', 'invests.user_id')->get()->toArray();
        $ids = [];
        foreach ($invests as $invest) {
            $ids[] = $invest['id'];
        }
        return $ids;
    }
}
