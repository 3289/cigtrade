<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Request;

class LangServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        $locale = 'ru';
        $url = Request::instance()->server->get('REQUEST_URI');
        if (strlen(Request::segment(1)) == 2)
        {
            $locale = Request::segment(1);
            \App::setLocale($locale);
        } else if (strpos($url,'admin') == false
            && strpos($url,'_debugbar') == false
            && strpos($url,'ajax') == false
            && !\App::runningInConsole()) {
            header("Location: /$locale$url");
            die();
        }
    }

    public function register()
    {
        //
    }
}