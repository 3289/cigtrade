<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageTerm extends Model
{
    protected $table = "package_term";
    protected $fillable =
        [
            'term',
            'percents',
            'package_id'
        ];

    public function package()
    {
        return $this->belongsTo('App\Package', 'package_id');
    }
}
