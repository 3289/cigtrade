<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Payout extends Model
{
    protected $table = "payouts";
    protected $fillable = ["amount", "type", "done", "requisites", "comment", "term_id", "package_id", "invest_id"];

    public static function boot()
    {
        parent::boot();
        Payout::creating(function($item){
            $item->user_id = Auth::user()->id;
        });
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function package()
    {
        return $this->belongsTo('App\Package');
    }

    public function state()
    {
        return $this->done ? 'Done' : "Pending...";
    }
}
