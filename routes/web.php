<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$locale = Request::segment(1);

Route::group(['prefix' => 'ajax'], function() {
    Route::get('/categories/children/{id}/{elementName}/{except}', 'Admin\CategoriesController@renderChose');
    Route::get('/categories/check_children/{id}', 'Admin\CategoriesController@checkEndCategory');
});

Route::group(array('prefix' => $locale), function() {
    Route::group(['middleware' => ['web']], function () {
        Route::post('/nixmoney/success', 'PaymentController@nixmoney');
        Route::post('/nixmoney/fail', 'PaymentController@nixmoney_fail');

        Route::post('/cryptonator/success', 'PaymentController@cryptonator');
        Route::post('/cryptonator/fail', 'PaymentController@cryptonator_fail');

        Auth::routes();
        Route::get('/', 'HomeController@index');
        Route::get('/ref/{id}', 'HomeController@register');

        Route::group(array('prefix' => 'ajax'), function() {
            Route::get('payout/check/{id}', ['as' => 'payout.check', 'use' => 'PayoutController@allowPayout']);
        });
    });

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/home', 'ProfileController@home');
        Route::get('/profile', 'ProfileController@index');
        Route::get('/profile/invest/{id}', 'ProfileController@invest');
        Route::post('/profile/invest/make', 'ProfileController@make_invest');
        Route::get('/invests', 'ProfileController@invests');
        Route::get('/referals', 'ProfileController@referals');
        Route::get('/history/orders', 'ProfileController@orders');
        Route::get('/support', 'ProfileController@support');
        Route::post('/support/send', 'ProfileController@send');

        Route::post('/profile/user/update/{id}', 'ProfileController@update');

        Route::get('/products', 'ProductsController@index');
        Route::get('/signals', 'ProductsController@signals');
        Route::get('/product/{id}', 'ProductsController@item');
        Route::post('/product/buy', 'ProductsController@buy');

        Route::get('/payouts', 'PayoutController@index');
        Route::post('/payout/referal/order/', 'PayoutController@orderReferal');
        Route::post('/payout/referal/make', 'PayoutController@makeReferal');
    });
});

Route::group(['prefix' => 'admin'], function()
{
    Route::group(['middleware' => ['roles'], 'roles' => ['Administrators']], function () {
        Route::get('/', 'Admin\DashboardController@dashboard');
        Route::resource('/pages', 'Admin\PagesController');
        Route::resource('/users', 'Admin\UsersController');
        Route::resource('/roles', 'Admin\RolesController');
        Route::resource('/packages', 'Admin\PackagesController');
        Route::resource('/products', 'Admin\ProductsController');
        Route::resource('/categories','Admin\CategoriesController');

        Route::post('/user/block/{id}', 'Admin\UsersController@block');
        Route::post('/user/unblock/{id}', 'Admin\UsersController@unBlock');

        Route::get('/orders', 'Admin\OrdersController@index');
        Route::get('/order/{id}', 'Admin\OrdersController@item');
        Route::post('/order/confirm', 'Admin\OrdersController@confirm');

        Route::get('/payouts', 'Admin\PayoutsController@index');
        Route::get('/payouts/{id}', 'Admin\PayoutsController@item');
        Route::post('/payouts/confirm', 'Admin\PayoutsController@confirm');
        Route::get('/invests', 'Admin\InvestsController@index');

        Route::get('/charts/users/{type?}/{month?}/{year?}', 'Admin\StatisticsController@users');
        Route::get('/charts/userterms/{type?}/{month?}/{year?}', 'Admin\StatisticsController@userTerms');
        Route::get('/charts/userpackages/{type?}/{month?}/{year?}', 'Admin\StatisticsController@userPackages');
    });
});

Route::group(['middleware' => ['roles'], 'roles' => ['Administrators'], 'prefix' => 'ajax'], function () {
    Route::post('/users/getPredictions', 'Admin\UsersController@getPredictions');
    Route::post('/roles/getPredictions', 'Admin\RolesController@getPredictions');
    Route::post('/roles/deprive', 'Admin\RolesController@deprive');
    Route::post('/roles/give', 'Admin\RolesController@give');

    Route::post('/term/add', 'Admin\PackagesController@add_term');
    Route::post('/term/remove', 'Admin\PackagesController@remove_term');
});
